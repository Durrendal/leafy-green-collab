;; charactergen.lisp -- Player Character Generator for Nameless

;; Copyright (C) Will Sinatra, 2019

;; Author: Will Sinatra <wpsinatra@gmail.com>

;;; Commentary:

;; None yet!

;;; Dependencies:

;; None yet!

;;;; Code:

(defpackage #:charactergen
  (:use #:cl)
  (:export :info-create
	   :p-name
	   :p-class
	   :p-race
	   :p-land
	   :prompt
	   :make-char
	   :*human*
	   :*elf*
	   :*dwarf*
	   :*ork*
	   :*troll*))

(in-package #:charactergen)

;; Player Attributes
(defvar p-name NIL)
(defvar p-class NIL)
(defvar p-race NIL)
(defvar p-race-n NIL)
(defvar p-land NIL)

;; Attribute Definitions

(defclass char-attr ()
  ((BOD-CUR
    :initarg :BOD-CUR)
   (BOD-MIN
    :initarg :BOD-MIN)
   (BOD-MAX
    :initarg :BOD-MAX)
   (AGI-CUR
    :initarg :AGI-CUR)
   (AGI-MIN
    :initarg :AGI-MIN)
   (AGI-MAX
    :initarg :AGI-MAX)
   (REA-CUR
    :initarg :REA-CUR)
   (REA-MIN
    :initarg :REA-MIN)
   (REA-MAX
    :initarg :REA-MAX)
   (STR-CUR
    :initarg :STR-CUR)
   (STR-MIN
    :initarg :STR-MIN)
   (STR-MAX
    :initarg :STR-MAX)
   (WIL-CUR
    :initarg :WIL-CUR)
   (WIL-MIN
    :initarg :WIL-MIN)
   (WIL-MAX
    :initarg :WIL-MAX)
   (LOG-CUR
    :initarg :LOG-CUR)
   (LOG-MIN
    :initarg :LOG-MIN)
   (LOG-MAX
    :initarg :LOG-MAX)
   (INT-CUR
    :initarg :INT-CUR)
   (INT-MIN
    :initarg :INT-MIN)
   (INT-MAX
    :initarg :INT-MAX)
   (CHA-CUR
    :initarg :CHA-CUR)
   (CHA-MIN
    :initarg :CHA-MIN)
   (CHA-MAX
    :initarg :CHA-MAX)
   (EDG-CUR
    :initarg :EDG-CUR)
   (EDG-MIN
    :initarg :EDG-MIN)
   (EDG-MAX
    :initarg :EDG-MAX)
   (ESS-CUR
    :initarg :ESS-CUR)
   (ESS-MIN
    :initarg :ESS-MIN)
   (ESS-MAX
    :initarg :ESS-MAX)
   (INI-CUR
    :initarg :INI-CUR)
   (INI-MIN
    :initarg :INI-MIN)
   (INI-MAX
    :initarg :INI-MAX)
   (MAG-CUR
    :initarg :MAG-CUR)
   (MAG-MIN
    :initarg :MAG-MIN)
   (MAG-MAX
    :initarg :MAG-MAX)
   (RES-CUR
    :initarg :RES-CUR)
   (RES-MIN
    :initarg :RES-MIN)
   (RES-MAX
    :initarg :RES-MAX)))

;; Base Attributes by SR5e Race
(defparameter *human*
  (make-instance 'char-attr
		 :BOD-CUR NIL
		 :BOD-MIN 1
		 :BOD-MAX 6
		 :AGI-CUR NIL
		 :AGI-MIN 1
		 :AGI-MAX 6
		 :REA-CUR NIL
		 :REA-MIN 1
		 :REA-MAX 6
		 :STR-CUR NIL
		 :STR-MIN 1
		 :STR-MAX 6
		 :WIL-CUR NIL
		 :WIL-MIN 1
		 :WIL-MAX 6
		 :LOG-CUR NIL
		 :LOG-MIN 1
		 :LOG-MAX 6
		 :INT-CUR NIL
		 :INT-MIN 1
		 :INT-MAX 6
		 :CHA-CUR NIL
		 :CHA-MIN 1
		 :CHA-MAX 6
		 :EDG-CUR 2
		 :EDG-MIN 0
		 :EDG-MAX 7
		 :ESS-CUR 6
		 :ESS-MIN 0
		 :ESS-MAX 6
		 :INI-CUR NIL
		 :INI-MIN 2
		 :INI-MAX 12))

(defparameter *elf*
  (make-instance 'char-attr
		 :BOD-CUR NIL
		 :BOD-MIN 1
		 :BOD-MAX 6
		 :AGI-CUR NIL
		 :AGI-MIN 2
		 :AGI-MAX 7
		 :REA-CUR NIL
		 :REA-MIN 1
		 :REA-MAX 6
		 :STR-CUR NIL
		 :STR-MIN 1
		 :STR-MAX 6
		 :WIL-CUR NIL
		 :WIL-MIN 1
		 :WIL-MAX 6
		 :LOG-CUR NIL
		 :LOG-MIN 1
		 :LOG-MAX 6
		 :INT-CUR NIL
		 :INT-MIN 1
		 :INT-MAX 6
		 :CHA-CUR NIL
		 :CHA-MIN 3
		 :CHA-MAX 8
		 :EDG-CUR 1
		 :EDG-MIN 0
		 :EDG-MAX 6
		 :ESS-CUR 6
		 :ESS-MIN 0
		 :ESS-MAX 6
		 :INI-CUR NIL
		 :INI-MIN 2
		 :INI-MAX 12))

(defparameter *dwarf*
  (make-instance 'char-attr
		 :BOD-CUR NIL
		 :BOD-MIN 3
		 :BOD-MAX 8
		 :AGI-CUR NIL
		 :AGI-MIN 1
		 :AGI-MAX 6
		 :REA-CUR NIL
		 :REA-MIN 1
		 :REA-MAX 5
		 :STR-CUR NIL
		 :STR-MIN 3
		 :STR-MAX 8
		 :WIL-CUR NIL
		 :WIL-MIN 2
		 :WIL-MAX 7
		 :LOG-CUR NIL
		 :LOG-MIN 1
		 :LOG-MAX 6
		 :INT-CUR NIL
		 :INT-MIN 1
		 :INT-MAX 6
		 :CHA-CUR NIL
		 :CHA-MIN 1
		 :CHA-MAX 6
		 :EDG-CUR 1
		 :EDG-MIN 0
		 :EDG-MAX 6
		 :ESS-CUR 6
		 :ESS-MIN 0
		 :ESS-MAX 6
		 :INI-CUR NIL
		 :INI-MIN 2
		 :INI-MAX 11))

(defparameter *ork*
  (make-instance 'char-attr
		 :BOD-CUR NIL
		 :BOD-MIN 4
		 :BOD-MAX 9
		 :AGI-CUR NIL
		 :AGI-MIN 1
		 :AGI-MAX 6
		 :REA-CUR NIL
		 :REA-MIN 1
		 :REA-MAX 6
		 :STR-CUR NIL
		 :STR-MIN 3
		 :STR-MAX 8
		 :WIL-CUR NIL
		 :WIL-MIN 1
		 :WIL-MAX 6
		 :LOG-CUR NIL
		 :LOG-MIN 1
		 :LOG-MAX 5
		 :INT-CUR NIL
		 :INT-MIN 1
		 :INT-MAX 6
		 :CHA-CUR NIL
		 :CHA-MIN 1
		 :CHA-MAX 5
		 :EDG-CUR 1
		 :EDG-MIN 0
		 :EDG-MAX 6
		 :ESS-CUR 6
		 :ESS-MIN 0
		 :ESS-MAX 6
		 :INI-CUR NIL
		 :INI-MIN 2
		 :INI-MAX 12))

(defparameter *troll*
  (make-instance 'char-attr
		 :BOD-CUR NIL
		 :BOD-MIN 5
		 :BOD-MAX 10
		 :AGI-CUR NIL
		 :AGI-MIN 1
		 :AGI-MAX 5
		 :REA-CUR NIL
		 :REA-MIN 1
		 :REA-MAX 6
		 :STR-CUR NIL
		 :STR-MIN 5
		 :STR-MAX 10
		 :WIL-CUR NIL
		 :WIL-MIN 1
		 :WIL-MAX 6
		 :LOG-CUR NIL
		 :LOG-MIN 1
		 :LOG-MAX 5
		 :INT-CUR NIL
		 :INT-MIN 1
		 :INT-MAX 5
		 :CHA-CUR NIL
		 :CHA-MIN 1
		 :CHA-MAX 4
		 :EDG-CUR 1
		 :EDG-MIN 0
		 :EDG-MAX 6
		 :ESS-CUR 6
		 :ESS-MIN 0
		 :ESS-MAX 6
		 :INI-CUR NIL
		 :INI-MIN 2
		 :INI-MAX 11))

;; Skills

;; General Operational Functions
(defun prompt (line)
  (format t "~a~%" line))

(defun print-sheet (obj)
  (pprint (list ":BOD" (slot-value obj 'BOD-CUR)
	       ":AGI" (slot-value obj 'AGI-CUR)
	       ":REA" (slot-value obj 'REA-CUR)
	       ":STR" (slot-value obj 'STR-CUR)
	       ":WIL" (slot-value obj 'WIL-CUR)
	       ":LOG" (slot-value obj 'LOG-CUR)
	       ":INT" (slot-value obj 'INT-CUR)
	       ":CHA" (slot-value obj 'CHA-CUR)
	       ":EDG" (slot-value obj 'EDG-CUR)
	       ":ESS" (slot-value obj 'ESS-CUR)
	       ":INI" (slot-value obj 'INI-CUR))))

(defun make-char (race)
  (setf (slot-value race 'BOD-CUR) (+ (slot-value race 'BOD-MIN) (random (- (slot-value race 'BOD-MAX) (slot-value race 'BOD-MIN)))))
  (setf (slot-value race 'AGI-CUR) (+ (slot-value race 'AGI-MIN) (random (- (slot-value race 'AGI-MAX) (slot-value race 'AGI-MIN)))))
  (setf (slot-value race 'REA-CUR) (+ (slot-value race 'REA-MIN) (random (- (slot-value race 'REA-MAX) (slot-value race 'REA-MIN)))))
  (setf (slot-value race 'STR-CUR) (+ (slot-value race 'STR-MIN) (random (- (slot-value race 'STR-MAX) (slot-value race 'STR-MIN)))))
  (setf (slot-value race 'WIL-CUR) (+ (slot-value race 'WIL-MIN) (random (- (slot-value race 'WIL-MAX) (slot-value race 'WIL-MIN)))))
  (setf (slot-value race 'LOG-CUR) (+ (slot-value race 'LOG-MIN) (random (- (slot-value race 'LOG-MAX) (slot-value race 'LOG-MIN)))))
  (setf (slot-value race 'INT-CUR) (+ (slot-value race 'INT-MIN) (random (- (slot-value race 'INT-MAX) (slot-value race 'INT-MIN)))))
  (setf (slot-value race 'CHA-CUR) (+ (slot-value race 'CHA-MIN) (random (- (slot-value race 'CHA-MAX) (slot-value race 'CHA-MIN)))))
  (setf (slot-value race 'EDG-CUR) (+ (slot-value race 'EDG-MIN) (random (- (slot-value race 'EDG-MAX) (slot-value race 'EDG-MIN)))))
  (setf (slot-value race 'ESS-CUR) (+ (slot-value race 'ESS-MIN) (random (- (slot-value race 'ESS-MAX) (slot-value race 'ESS-MIN)))))
  (setf (slot-value race 'INI-CUR) (+ (slot-value race 'REA-CUR) (slot-value race 'INT-CUR)))
  (print-sheet race))
    
;; Character Generation Prompt
(defun info-create ()
  (prompt "What is your name?")
  (setf p-name (read))
  (prompt "Class:")
  (format t "1) Decker  2) Street Samurai~%3) Rigger  4) Mage~%5) Shaman  6) Adept~%7) Face  8) Technomancer~%")
  (let
      ((c-input (read)))
    (cond
      ((equal c-input 1)
       (setf p-class "Decker"))
      ((equal c-input 2)
       (setf p-class "Street-Samurai"))
      ((equal c-input 3)
       (setf p-class "Rigger"))
      ((equal c-input 4)
       (setf p-class "Mage"))
      ((equal c-input 5)
       (setf p-class "Shaman"))
      ((equal c-input 6)
       (setf p-class "Adept"))
      ((equal c-input 7)
       (setf p-class "Face"))
      ((equal c-input 8)
       (setf p-class "Technomancer"))))
  (prompt "Race:")
  (format t "1) Human  2) Elf~%3) Dwarf  4) Ork~%5) Troll~%")
  (let
      ((r-input (read)))
    (cond
      ((equal r-input 1)
       (setf p-race *human*)
       (setf p-race-n "Human"))
      ((equal r-input 2)
       (setf p-race *elf*)
       (setf p-race-n "Elf"))
      ((equal r-input 3)
       (setf p-race *dwarf*)
       (setf p-race-n "Dwarf"))
      ((equal r-input 4)
       (setf p-race *ork*)
       (setf p-race-n "Ork"))
      ((equal r-input 5)
       (setf p-race *troll*)
       (setf p-race-n "Troll"))))
  (prompt "From where do you hail?")
  (setf p-land (read))
  (format t "So you are ~a, a ~a ~a hailing from ~a~%" p-name p-race-n p-class p-land)
  (make-char p-race))
  
