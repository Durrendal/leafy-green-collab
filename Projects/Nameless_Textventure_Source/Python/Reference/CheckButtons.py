#!/usr/bin/env python

from tkinter import *


class Application(Frame):
    # GUI #

    def __init__(self, master):
        # initializes frame #
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets() # create widgets method


    def create_widgets(self):
        # Create widgets #
        Label(self, text = "Choose option").grid(row = 0, column = 0, sticky = W)

        Label(self, text = "Select all that apply: ").grid(row = 1, column = 0, sticky = W)

        # options as checkboxes #
        self.option1 = BooleanVar()
        Checkbutton(self, text = "Option1", variable
        = self.option1, command = self.update_text).grid(row = 2, column = 0, sticky = W)

        self.option2 = BooleanVar()
        Checkbutton(self, text = "Option2", variable
        = self.option2, command = self.update_text).grid(row = 3, column = 0, sticky = W)

        self.option3 = BooleanVar()
        Checkbutton(self, text = "Option3", variable
        = self.option3, command = self.update_text).grid(row = 4, column = 0, sticky = W)

        self.result = Text(self, width = 40, height = 5, wrap = WORD)
        self.result.grid(row = 5, column = 0, columnspan = 3)


    def update_text(self):
        # updates text #
        likes = ""

        if self.option1.get():
            likes += "option 1 selected"
        elif self.option2.get():
            likes += "option 2 selected"
        elif self.option3.get():
            likes += "option 3 selected"
        self.result.delete(0.0, END)
        self.result.insert(0.0, likes)


# tkinter variables
root = Tk()
root.title("GUI - Check Buttons")
root.geometry("500x500")
app = Application(root)
root.mainloop()
