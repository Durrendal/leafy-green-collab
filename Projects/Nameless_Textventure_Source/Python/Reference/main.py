from classes.game import Person, colors

#spell list
mag = [{"name": "Fire", "cost": 10, "dmg": 60},
       {"name": "Thunder", "cost": 10, "dmg": 70},
       {"name": "Blizzard", "cost": 10, "dmg": 50}]

#player attributes and damage
player = Person(460, 65, 60, 34, mag)
print(player.generate_spell_damage(0))
print(player.generate_spell_damage(1))