#!/usr/bin/env python

__author__ = "KBSego"
__copyright__ = "Copyright (c) 2018, KBSego"
__version__ = "1.0.0"
###################
# PyCharSheet v1  #
# Using tkinter   #
###################
from tkinter import *

##### Application Class #####
class Application(Frame):
    ##### GUI #####
    def __init__(self, master):
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets()

    ##### Create Widgets #####
    def create_widgets(self):
        self.instruction = Label(self,text="Enter character Info",font='Helvitica 16 bold').grid(
            row=0,column=1,columnspan=2,sticky=W)
        self.use = Label(self,text="""To use: Enter text on Left.\nPress Submit when done.\nText will populate on right""",
                         font='Helvitica 10 bold').grid(
                         row=1,column=1,columnspan=1,sticky=W)

        self.breaker = Label(
            self,text="#------------------------------------------------------------------------------------------------------------------------#",
            font='Helvitica 10').grid(row=2,column=0,columnspan=5,sticky=W)

        self.name_label = Label(self,text="Character Name: ",font='Helvitica 12 bold').grid(
            row=3,column=0,columnspan=2,sticky=W)

        self.character_name = Entry(self)
        self.character_name.grid(row=4,column=0,sticky=W)

        self.character_name_text = Text(self,width=15,height=1,wrap=WORD)
        self.character_name_text.grid(row=4,column=1,columnspan=1,sticky=W,padx=10)
        self.character_name_text.config(state=DISABLED)


        self.stats_label = Label(self,text="Stats: ",font='Helvitica 12 bold').grid(
            row=5,column=0,columnspan=2,sticky=W)

        # STR
        self.strength_label = Label(self,text="STR:",font='Helvitica 10').grid(
            row=6,column=0,columnspan=1,sticky=W,padx=1)
        self.strength = Entry(self,width=5)
        self.strength.grid(row=7,column=0,sticky=W)

        self.strength_text = Text(self,width=5,height=1,wrap=WORD)
        self.strength_text.grid(row=7,column=1,columnspan=1,sticky=W)
        self.strength_text.config(state=DISABLED)

        # DEX
        self.dexterity_label = Label(self,text="DEX:",font='Helvitica 10').grid(
            row=8,column=0,columnspan=1,sticky=W,padx=1)
        self.dexterity = Entry(self,width=5)
        self.dexterity.grid(row=9,column=0,sticky=W)

        self.dexterity_text = Text(self,width=5,height=1,wrap=WORD)
        self.dexterity_text.grid(row=9,column=1,columnspan=1,sticky=W)
        self.dexterity_text.config(state=DISABLED)

        # CON
        self.constitution_label = Label(self,text="CON:",font='Helvitica 10').grid(
            row=10,column=0,columnspan=1,sticky=W,padx=1)
        self.constitution = Entry(self,width=5)
        self.constitution.grid(row=11,column=0,sticky=W)

        self.constitution_text = Text(self,width=5,height=1,wrap=WORD)
        self.constitution_text.grid(row=11,column=1,columnspan=1,sticky=W)
        self.constitution_text.config(state=DISABLED)

        # INT
        self.intelligence_label = Label(self,text="INT:",font='Helvitica 10').grid(
            row=12,column=0,columnspan=1,sticky=W,padx=1)
        self.intelligence = Entry(self,width=5)
        self.intelligence.grid(row=13,column=0,sticky=W)

        self.intelligence_text = Text(self,width=5,height=1,wrap=WORD)
        self.intelligence_text.grid(row=13,column=1,columnspan=1,sticky=W)
        self.intelligence_text.config(state=DISABLED)

        # WIS
        self.wisdom_label = Label(self,text="WIS:",font='Helvitica 10').grid(
            row=14,column=0,columnspan=1,sticky=W,padx=1)
        self.wisdom = Entry(self,width=5)
        self.wisdom.grid(row=15,column=0,sticky=W)

        self.wisdom_text = Text(self,width=5,height=1,wrap=WORD)
        self.wisdom_text.grid(row=15,column=1,columnspan=1,sticky=W)
        self.wisdom_text.config(state=DISABLED)

        #CHA
        self.charisma_label = Label(self,text="CHA:",font='Helvitica 10').grid(
            row=16,column=0,columnspan=1,sticky=W,padx=1)
        self.charisma = Entry(self,width=5)
        self.charisma.grid(row=17,column=0,sticky=W)

        self.charisma_text = Text(self,width=5,height=1,wrap=WORD)
        self.charisma_text.grid(row=17,column=1,columnspan=1,sticky=W)
        self.charisma_text.config(state=DISABLED)

        self.breaker2 = Label(
            self,text="#------------------------------------------------------------------------------------------------------------------------#",
            font='Helvitica 10').grid(row=18,column=0,columnspan=5,sticky=W)
        
        # SUBMIT VALUES
        self.button = Button(self,text="Submit Values",command=self.submit_text)
        self.button.grid(row=19,column=0,sticky=W)

    ##### Submit Entered Values To Text Fields #####
    def submit_text(self):
        put_name = self.character_name.get()

        self.character_name_text.config(state=NORMAL)
        self.character_name_text.delete(0.0, END) # Clear text
        self.character_name_text.insert(0.0, put_name)
        self.character_name_text.config(state=DISABLED)

# tkinter variables
root = Tk()
root.title("PY Character Sheet")
root.geometry("500x500")
root.resizable(False,False)
app = Application(root)
root.mainloop()
