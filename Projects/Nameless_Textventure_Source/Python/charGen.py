__author__ = "KBSego"
__copyright__ = "Copyright (c) 2018, KBSego"
__version__ = "1.0.0"


# PYCharGen

def main():
    # create character.txt with write+ perms
    file = open("character.txt","w+")
    
    name = input("What is your name: ")
    file.write("Character: " + name.upper() + "\n")
    file.close()

    # list classes
    print("""
          Available Classes:
          1: Fighter
          2: Mage
          3: Archer
          4: Thief
          """)

    role = input("What is your class: ")

    # determine class from input & append to file
    if role == "1":
        print("You chose the FIGHTER Class")
        file = open("character.txt","a+")
        file.write("Class: FIGHTER")
        file.close()
    elif role == "2":
        print("You chose the MAGE Class")
        file = open("character.txt","a+")
        file.write("Class: MAGE")
        file.close()
    elif role == "3":
        print("You chose the ARCHER Class")
        file = open("character.txt","a+")
        file.write("Class: ARCHER")
        file.close()
    elif role == "4":
        print("You chose the THIEF Class")
        file = open("character.txt","a+")
        file.write("Class: THIEF")
        file.close()
    else:
        print("Please choose a valid role")
        main()


    # call to dice roll?? Manual input -> list each attribute and tell what dice
    # send input to txt file or flat file for storage

if __name__ == "__main__":
    main()