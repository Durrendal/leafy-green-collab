# Leafy-Green-Collab
```
A collection of collaborative projects between:
Kaleb Sego & Will Sinatra & Kevin Sutor & Kenneth Faircloth
Also a repo for general knowledge sharing between Lisp, Python, and Rust

Current projects are:
The Nameless Textventure
```

## Lisp
All lisp source is written by Will Sinatra

## Python
All python source is written by Kaleb Sego

## Rust
All Rust source is written by Kevin Sutor  

## C++  
All C++ source is written by Kenneth Faircloth