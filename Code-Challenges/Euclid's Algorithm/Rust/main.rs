use std::io;

fn main() {
    let mut a = String::new();
    let mut b = String::new();
    let mut c = 1;
    println!("Enter 2 numbers and I'll give you the D...the GCD");

    io::stdin().read_line(&mut a)
        .expect("fAiLeD tO rEaD lInE");
    let a: i128 = match a.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };

    io::stdin().read_line(&mut b)
        .expect("fAiLeD tO rEaD lInE");
    let b: i128 = match b.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };

    let mut x = 2;
    
    while x <= a && x <= b {
        if a % x == 0 && b % x ==0 {
            c = x;
        }
        x+=1;
    }
    println!("The Greatest Common Denominator is {}", c);
    again()
}

fn again() {
    let mut mo = String::new();
    println!("\nWould you like to enter a different pair of numbers? (y/n)");
    io::stdin().read_line(&mut mo)
        .expect("Failed to Read Line");
    match mo.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => again(),
    };
}
