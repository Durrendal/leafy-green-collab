use std::io::{self, Write};
use rand::Rng;

fn main() {
    let mut input = String::new();
    let mut done = String::new();
    let _reply = ["Yeah fo sho'",
                  "Just. Do It.",
                  "Believe It Dattebayo!",
                  "IDK why don't you ask the 9 ball",
                  "That's a dumb question",
                  "Eeeeeeehhhhhh",
                  "Nah bruh",
                  "Doubt",
                  "Lol fuck that"];

    loop {
        println!("Ask the Magical 8 Ball a question:");

        io::stdin().read_line(&mut input)
            .expect("Failed to read line");

        println!("{}",_reply[rand::thread_rng().gen_range(0,_reply.len())]);

        print!("Would you like to play again? (y/n): ");
        io::stdout().flush().unwrap();

        io::stdin().read_line(&mut done)
            .expect("y or n");
        
        if done.trim() == "n" {
            println!("kbye");
            break;
        } else if done.trim() == "y" {
            done = "".to_string();
            println!("");
            continue;
        } else {
            println!("No games for people who can't spell");
            break;
        }
    }
}
