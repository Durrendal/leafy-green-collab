import std/random

#Procs are just functions
proc main(): string =
  #Set randomization
  randomize()
  #Define a sequence of answers
  let resp = ["It is certain", "It is decidedly so", "Yes definitely", "Reply hazy try again", "Ask again later", "My reply is no good", "Outlook not so good", "Very doubtful"]
  #Our response is an index from the sequence, indexes start a 0 in nim
  let response = rand(resp.len - 1)
  # We return the string, because the proc must return such
  resp[response]

#Proc called, and echo'd to stdout, which consumes the string. This prevents us from needing to discard main() the proc
echo main()
