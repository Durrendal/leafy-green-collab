package main

import (
	"fmt"
	"time"
	"math/rand"
)

var responses = []string{"It is certain", "It is decidedly so", "Yes definitely", "Reply hazy try again", "Ask again later", "My reply is no good", "Outlook not so good", "Very doubtful"}

func main () {
	seed := rand.NewSource(time.Now().UnixNano())
	handler := rand.New(seed)
	rnum := handler.Intn(len(responses))

	fmt.Println(responses[rnum])
}
