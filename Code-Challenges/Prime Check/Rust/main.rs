use std::io;

fn main() {
    let mut input = String::new();
    let mut x = 2;
    let mut prime = true;
    println!("Pick a number and I'll tell you if it's prime!");

    io::stdin().read_line(&mut input)
        .expect("Failed to Read Line");
    let input: i128 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };
    
    while x < input {
        if input % x == 0 {
            println!("{} is divisible by {}", input, x);
            prime = false;
        }
        x += 1;
    }

    if prime {
        println!("Yup, {} is indeed a prime number", input);
    }
    again()
}

fn again() {
    let mut mo = String::new();
    println!("\nWould you like to check if another number is prime? (y/n)");
    io::stdin().read_line(&mut mo)
        .expect("Failed to Read Line");
    match mo.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => again(),
    };
}
