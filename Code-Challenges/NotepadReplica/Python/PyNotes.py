import tkinter
import os
from tkinter.messagebox import *
from tkinter.filedialog import *
from PIL import Image  # pil is now pillow, import unchanged


class Notepad:

    root = Tk()

    # default width + height + menu stuff
    dWidth = 300
    dHeight = 300
    textArea = Text(root)
    menuBar = Menu(root)
    fileMenu = Menu(menuBar, tearoff=0)
    editMenu = Menu(menuBar, tearoff=0)
    helpMenu = Menu(menuBar, tearoff=0)

    # scrollbar
    scrollBar = Scrollbar(textArea)
    __file = None

    def __init__(self, **kwargs):

        # icon mooooo
        try:
            self.root.wm_iconbitmap("cow.ico")
        except:  # lazy except bc image might not be here
            pass

        # set window size (the default is 300x300)

        try:
            self.dWidth = kwargs['width']
        except KeyError:
            pass

        try:
            self.dHeight = kwargs['height']
        except KeyError:
            pass

        # set titlebar text
        self.root.title("Untitled - PyNotes")

        # center window
        screenWidth = self.root.winfo_screenwidth()
        screenHeight = self.root.winfo_screenheight()

        # left align
        left = (screenWidth / 2) - (self.dWidth / 2)

        # right align
        top = (screenHeight / 2) - (self.dHeight /2)

        # top and bottom geometry
        self.root.geometry('%dx%d+%d+%d' %
                           (self.dWidth, self.dHeight, left, top))

        # resizing textarea
        self.root.grid_rowconfigure(0, weight=1)
        self.root.grid_columnconfigure(0, weight=1)

        # controls - using sticky
        self.textArea.grid(sticky= N + E + S + W)

        # new file menu option
        self.fileMenu.add_command(label="New", command=self.newFile)

        # open file menu option
        self.fileMenu.add_command(label="Open", command=self.openFile)

        # save file menu option
        self.fileMenu.add_command(label="Save", command=self.saveFile)

        # line seperator for file meun -> seperates Exit from others
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.quitApplication)
        self.menuBar.add_cascade(label="File", menu=self.fileMenu)

        # cut
        self.editMenu.add_command(label="Cut", command=self.__cut)

        # copy
        self.editMenu.add_command(label="Copy", command=self.__copy)

        # paste
        self.editMenu.add_command(label="Paste", command=self.__paste)

        # edit menu
        self.menuBar.add_cascade(label="Edit", menu=self.editMenu)

        # about dialog + help menu + image info
        # image is a cow mooo
        self.helpMenu.add_command(label="What's the icon?", command=self.showImage)
        self.helpMenu.add_command(label="About Notepad", command=self.showAbout)
        self.menuBar.add_cascade(label="Help", menu=self.helpMenu)

        self.root.config(menu=self.menuBar)

        self.scrollBar.pack(side=RIGHT, fill=Y)

        # scrollbar adjust
        self.scrollBar.config(command=self.textArea.yview)
        self.textArea.config(yscrollcommand=self.scrollBar.set)

    def quitApplication(self):
        # self.root.destroy() -> destroys widgets + ends mainloop
        quit()  # lazy quit

    def showAbout(self):
        showinfo("Notepad Replica - PyNotes", "Just a nifty notepad replica\nKBSego2019")

    def showImage(self):
        try:
            Image.open("cow.ico").show()
        except:  # lazy except bc image might not be here
            pass

    def openFile(self):

        self.__file = askopenfilename(defaultextension=".txt",
            filetypes=[("All Files", "*.*"),
            ("Text Documents", "*.txt")])

        if self.__file == "":

            # try to open a file
            self.__file = None
        else:

            # open a file and set title to file - Notepad
            self.root.title(os.path.basename(self.__file) + " - Notepad")
            self.textArea.delete(1.0, END)

            file = open(self.__file,"r")

            self.textArea.insert(1.0, file.read())

            file.close()

    # new file titlebar = Untitled
    def newFile(self):
        self.root.title("Untitled - Notepad")
        self.__file = None
        self.textArea.delete(1.0, END)

    def saveFile(self):

        if self.__file == None:
            # save handling
            self.__file = asksaveasfilename(initialfile='Untitled.txt',
                defaultextension=".txt",
                filetypes=[("All Files","*.*"),
                ("Text Documents","*.txt")])

            if self.__file == "":
                self.__file = None
            else:

                # save
                file = open(self.__file,"w")
                file.write(self.textArea.get(1.0, END))
                file.close()

                # update the title
                self.root.title(os.path.basename(self.__file) + " - Notepad")

        else:
            file = open(self.__file,"w")
            file.write(self.textArea.get(1.0, END))
            file.close()

    def __cut(self):
        self.textArea.event_generate("<<Cut>>")

    def __copy(self):
        self.textArea.event_generate("<<Copy>>")

    def __paste(self):
        self.textArea.event_generate("<<Paste>>")

    def run(self):

        # mainloop
        self.root.mainloop()


# run application - set width and height
notepad = Notepad(width=600, height=700)
notepad.run()
