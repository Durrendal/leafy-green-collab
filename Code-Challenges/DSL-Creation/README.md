For this challenge we will be creating our own Domain Specific Language.

We want to program in our language of choice, end of story. But in real life sometimes jobs require us to program in languages we aren't as familiar with, or rather dislike for various reasons. So why should we ever leave our languages of choice then?

Write a program that writes a functional hello world program in a different language, two if you feel ambitious, I've already done a Lisp to Rust conversion of "Hello World" and will do a Python one next, make them dynamic so that you can use the same functions to expand upon later.