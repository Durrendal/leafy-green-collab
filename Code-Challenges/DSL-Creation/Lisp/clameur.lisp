;; Author: Will Sinatra <wpsinatra@gmail.com>
;; ===== Lisp like Rust =====
(defmacro fn (name args &rest body)
  "Simple macro to cause (fn) to act as (defun)"
  `(defun ,name ,args
     (progn ,@body)))

(defun println! (x)
  "Simple function to call format t with println! instead"
  (format t "~a~%" x))

;; Useage/Example:
;;fn main () {
;;      println!("Hello World");
;;}

;; (fn main ()
;;  (println! "Hello World"))

;; ===== Lisp to Rust Conversion =======
(defmacro r/fn (name args &rest body)
  "Macro that writes a rust style fn x () { line"
  `(concatenate 'string
		"fn " ,name ,args " { " '(#\Newline) ,@body '(#\Newline) "}"))

(defmacro r/cfn (name args)
  `(concatenate 'string ,name"(",args")"";"))

(defmacro r/if (condname condargs condact &optional &rest body)
  `(concatenate 'string "if " ,condname,condargs " {" '(#\Newline) ,condact ,@body))

(defmacro r/elif (condname condargs condact)
  `(concatenate 'string "} else if " ,condname,condargs " {" '(#\Newline) ,condact))

(defmacro r/else (condact)
  `(concatenate 'string "} else {" '(#\Newline) ,condact "}"))

(defmacro r/for (forx conditional action)
  `(concatenate 'string "for " ,forx " in " ,conditional " {" '(#\Newline) ,action '(#\Newline) "}"))

(defmacro r/ret (args)
  `(concatenate 'string "return " ,args";"))

(defmacro r/expr (args conditional)
  `(concatenate 'string ,args ,conditional))

(defmacro r/println! (args &optional compargs)
  "Macro that writes a Rust style printlin! function"
  `(concatenate 'string
		"println!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline)))

(defmacro r/} (&optional pad)
  `(if (equal ,pad t)
       (concatenate 'string "}" '(#\Newline))
       (concatenate 'string " }")))

(defmacro r/nl (pad args)
  `(cond
     ((equal ,pad "b")
      (concatenate 'string '(#\Newline) ,args '(#\Newline)))
     ((equal ,pad "l")
      (concatenate 'string '(#\Newline) ,args))
     ((equal ,pad "r")
      (concatenate 'string ,args '(#\Newline)))))

(defmacro l->r (&optional io args &rest body)
  "Simple start to a lisp to rust converter"
  `(if (equal ,io t)
       (let
	   ((in (open "main.rs" :direction :output :if-exists :supersede)))
	 (format in "~a" (concatenate 'string
				       '(#\Newline) ,args ,@body '(#\Newline)))
	 (close in))
       (concatenate 'string
		    '(#\Newline) ,args ,@body '(#\Newline))))

;; Usage/Example:
;;* (l->r
;;   (r/fn "main")
;;   (r/println! "Hello World, this is a lisp to Rust converter!"))
 
;; "fn main () { println!(\"Hello World, this is a lisp to Rust converter!\"); }"

(l->r t
      (r/fn "main" "()"
	    (r/cfn "fizzbuzz_to" "100"))
      (r/nl "l" (r/fn "is_divisible_by" "(lhs: u32, rhs: u32) -> bool"
		      (r/if "rhs == " "0"
			    (r/nl "r" (r/ret "false"))
			    (r/} t))
		      (r/expr "lhs % rhs == " "0")))
      (r/nl "l" (r/fn "fizzbuzz" "(n: u32) -> ()"
		      (r/if "is_divisible_by" "(n, 15)" (r/println! "fizzbuzz")
			    (r/elif "is_divisible_by" "(n, 3)" (r/println! "fizz"))
			    (r/elif "is_divisible_by" "(n, 5)" (r/println! "buzz"))
			    (r/else (r/println! "{}" ", n")))))
      (r/nl "l" (r/fn "fizzbuzz_to" "(n: u32)"
		      (r/for "n" "1..n +1" "fizzbuzz(n);"))))
      
