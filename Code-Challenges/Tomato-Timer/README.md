# Challenge:
Create a "Pomodoro" style timer system.

- Default to 25min timers in 4 sets
- Allow users to denote a custom time if they want
- Make sure the users can denote the time in minutes so they don't have to do math to use your program

Bonus:
- Add an inline TODO list