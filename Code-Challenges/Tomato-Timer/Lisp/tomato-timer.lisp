;; Author: Will Sinatra <wpsinatra@gmail.com>
(defpackage #:tomato-timer
  (:use :cl :uiop)
  (:export :main))

(in-package tomato-timer)

(defvar t-set 1)
(defvar t-todo 0)
(defvar mtodo 0)
(defparameter *todo* (make-hash-table))

(defun convert-m-to-s (val)
  (* val 60))

;; Doug Hoyte's unit-of-time macro, simplified slightly
(defmacro time-convert (val typ)
  `(* ,val
      ,(case typ
	 ((s) 1)
	 ((m) 60)
	 ((h) 3600)
	 ((d) 86400))))

(defmacro time-invert (val typ)
  `(/ ,val
      ,(case typ
	 ((s) 1)
	 ((m) 60)
	 ((h) 3600)
	 ((d) 86400))))

(defun val-to-list (val)
  (let
      ((expanse () ))
    (dotimes (number val (nreverse expanse))
      (push number expanse))))

(defun print-todo ()
  (loop for i in (val-to-list (+ t-todo 1))
     do (format t "- [ ] ~a~%" (gethash i *todo*))))

(defun add-todo (desc)
  (incf t-todo)
  (let
      ((title (print t-todo)))
    (setf (gethash title *todo*) desc)))

(defun del-todo (title)
  (remhash title *todo*)
  (decf t-todo))

(defun tomato (&optional (time 1500))
  (when (= t-set 1)
    (format t "Would you like to add any TODO items?~%")
    (let
	((todo? (y-or-n-p)))
      (if (equal todo? t)
	  (loop while (equal 0 mtodo)
	     do (progn
		  (format t "Add a brief desciption of your task below~%")
		  (let
		      ((desc (read)))
		    (add-todo desc)))
	       (progn
		 (format t "Another?~%")
		 (if (equal (y-or-n-p) NIL)
		     (setf mtodo 1)))))
      (when (>= t-todo 1)
	(print-todo))))
  (when (> t-set 1)
    (format t "Did you complete any tasks?~%")
    (let
	((complete? (y-or-n-p)))
      (if (equal complete? t)
	  (progn
	    (format t "Which task did you complete? Enter that task number below~%")
	    (let
		((title (read)))
	      (del-todo title))
	    (print-todo)))))
  (let
      ((ttime (time-convert time m)))
    (if (equal (time-invert time s) 1)
	(format t "Your tomato will end in 1 minute.~%")
	(format t "Your tomato timer will end in ~a minutes.~%" (time-invert time s)))
    (sleep ttime)
    (when (>= t-set 4)
      (progn
	(format t "Take a five minute break, you've earned it!~%")
	(quit)))
    (format t "Your tomato has ended. Start another?~%")
    (let
	((next (y-or-n-p)))
      (cond
	((equal next t)
	 (incf t-set)
	 (tomato time))
	((equal next NIL)
	 (format t "Hopefully you've been productive, have a great day!~%"))))))
  
(opts:define-opts
    (:name :help
	   :description "Prints this helpful help message"
	   :short #\h
	   :long "help")
    (:name :convert
	   :description "Converts integer to seconds (ie: 25min = 1500 sec)"
	   :short #\c
	   :long "convert"
	   :arg-parser #'parse-integer)
  (:name :time
	 :description "Denote custom time"
	 :short #\t
	 :long "time"
	 :arg-parser #'parse-integer)
  (:name :atodo
	 :description "Adds item to be completed inside of target time"
	 :short #\a
	 :long "add-todo"
	 :arg-parser #'identity)
  (:name :dtodo
	 :description "Deletes item from todo"
	 :short #\d
	 :long "del-todo"
	 :arg-parser #'pprint))

(defun unknown-option (condition)
  (format t "warning: ~s is invalid!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun main ()
  (multiple-value-bind (options)
      (handler-case
	  (handler-bind ((opts:unknown-option #'unknown-option))
	    (opts:get-opts))
	(opts:missing-arg (condition)
	  (format t "fatal: option ~s needs an argument!~%"
		  (opts:option condition)))
	(opts:arg-parser-failed (condition)
	  (format t "fatal: cannot parse ~s as argument of ~s~%"
		  (opts:raw-arg condition)
		  (opts:option condition)))
	(opts:missing-required-option (con)
	  (format t "fatal: ~a~%" con)
	  (opts:exit 1)))
    (when-option (options :help)
		 (opts:describe
		  :prefix "Tomato-time -- MIT -- Author: Will Sinatra <wpsinatra@gmail.com>"
		  :usage-of "Tomato-time"))
    (when-option (options :convert)
		 (format t "Use -t ~a for your custom time~%" (convert-m-to-s (getf options :convert))))
    (when-option (options :time)
		 (progn
		   (print-todo)
		   (format t "~a~%" (tomato (getf options :time)))))
    (when-option (options :atodo)
		   (add-todo (getf options :atodo)))
    (when-option (options :dtodo)
		 (del-todo (getf options :dtodo)))))
