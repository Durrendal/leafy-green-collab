Make a number guessing game that assigns a random number between 1 and 20 (or whatever)
to a variable then let the user have 6 guesses to guess the number
Give indications as to if the number is too low, too high, if guessed right
or if the player failed