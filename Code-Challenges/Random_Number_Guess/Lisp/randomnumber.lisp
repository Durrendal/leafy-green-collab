;; Author: Will Sinatra <wpsinatra@gmail.com>

(defparameter solution 0)
(defvar guesses_taken 0)
(defvar int-str NIL)

(defun val-to-list (val)
  (let
      ((expanse () ))
    (dotimes (number val (nreverse expanse))
      (push number expanse))))

(defun str-expanse (val)
    (loop for i in (val-to-list val)
       collect (format NIL "~r" i)))

(defun concat-expanse (expanse val)
  (loop for word in expanse
     for val in (val-to-list val)
       collect (list word val)))

(defun ecase-build (expanse)
  (loop for item in expanse
     do (format t "~d~%" item)))

(defparameter case-val (progn
			 (ecase-build (concat-expanse (str-expanse 101) 101))))

    
(defun ecase-trim (expanse &optional item)
  (setf expanse (reverse expanse))
  (loop for e in expanse while (eq item e) do (pop expanse))
  (reverse expanse))

(defun str-to-val (str)
  (cond
    ((equal (typep str 'integer) NIL)
     (case str
       (case-vals)))
       ;(one 1)
       ;(two 2)
       ;(three 3)
       ;(four 4)
       ;(five 5)
       ;(six 6)
       ;(seven 7)
       ;(eight 8)
       ;(nine 9)
       ;(ten 10)
       ;(eleven 11)
       ;(twelve 12)
       ;(thirteen 13)
       ;(fourteen 14)
       ;(fifteen 15)
       ;(sixteen 16)
       ;(seventeen 17)
       ;(eighteen 18)
       ;(nineteen 19)
       ;(twenty 20)))
       ;(progn
	; (ecase-build (concat-expanse (str-expanse 101) 101))
	 ;(values))))
     ((equal (typep str 'integer) T)
      (push str int-str)
      (pop int-str))))
  
(defun guess-num ()
  (setf solution (random 20))
  (format t "I'm thinking of a number between 1 and 20~%")
  (loop
     (let
	 ((guess (read)))
       (cond
	 ((<= guesses_taken 7)
	  (let
	      ((guess-i (str-to-val guess)))
	    (cond
	      ((< guess-i solution)
	       (format t "Your guess was too low. Go higher!~%")
	       (incf guesses_taken))
	      ((> guess-i solution)
	       (format t "Your guess was too high. Go lower!~%")
	       (incf guesses_taken))
	      ((equal guess-i solution)
	       (format t "Congratulations! You guessed the number in ~a guesses!~%" guesses_taken)
	       (setf guesses_taken 0)
	       (format t "Would you like to play again?~%")
	       (let
		   ((again (y-or-n-p)))
		 (cond
		   ((equal again T)
		    (guess-num))
		   ((equal again NIL)
		    (format t "Thanks for playing!~%")
		    (quit))))))))
	 ((> guesses_taken 7)
	  (format t "Nope. The number was ~a~%" solution)
	  (format t "Would you like to play again?~%")
	  (let
	      ((again (y-or-n-p)))
	    (cond
	      ((equal again T)
	       (guess-num))
	      ((equal again NIL)
	       (format t "Thanks for playing!~%")
	       (quit)))))))))

;(guess-num)
