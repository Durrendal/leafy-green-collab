#!/usr/bin/fennel
;;Author: Will Sinatra <wpsinatra@gmail.com>

(fn tobin [num]
  (var bin "")
  (var num num)
  (while (~= num 0)
    (do
      (print num bin)
      (set bin (.. (% num 2) bin))
      (set num (math.modf (/ num 2)))))
  bin)

(print (tobin (tonumber (. arg 1))))
