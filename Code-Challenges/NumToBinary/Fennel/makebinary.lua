#!/usr/bin/env lua

local function tobin(num)
  local bin = ""
  local num0 = num
  while (num0 ~= 0) do
    print(num0, bin)
    bin = ((num0 % 2) .. bin)
    num0 = math.modf((num0 / 2))
  end
  return bin
end
return print(tobin(tonumber(arg[1])))
