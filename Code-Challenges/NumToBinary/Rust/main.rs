use std::io;

fn main() {
    let mut number = String::new();
    let dubs: u128 = 2;
    let mut x = 0;
    println!("Choose a whole  number you want to see a binary version of");

    io::stdin().read_line(&mut number)
        .expect("Failed to Read Line");
    let mut number: u128 = match number.trim().parse(){
        Ok(num) => num,
        Err(_) => 0,
    };
    print!("The binary number of {} is ", number);
    while number >= dubs.pow(x) {
        x += 1;
    }

    while x != 0 {
        x -= 1;
        if number >= dubs.pow(x) {
            print!("1");
            number -= dubs.pow(x);
        } else {
            print!("0");
        }
    }
    println!("");

    again();
}

fn again(){
    loop{
        println!("Convert another number? (y/n)");
        let mut replay = String::new();
        io::stdin().read_line(&mut replay)
            .expect("Failed to Read");
        match replay.trim() {
            "y" => main(),
            "Y" => main(),
            "n" => break,
            "N" => break,
            _ => continue,
        };
    }
}
