Write a set of functions to take a peak value, break it down into an array, and then iterate across that array to form a dataset, such that your output is

(zero 0)
(one 1)
(two 2)
("one hundred" 100)

The function should be dynamic such that one could pass 1, 100, 1000, or any other number ad nauseum to the program and it iterate in the same manner.