# one 1
"""
User inputs floor and ceiling or list
Use some for loop to iterate through nums
if user says 5 = floor and 80 = ceiling
then pgm should for loop from 5 to 80
using the floor and ceiling as range
include end range so stop+1
"""
import inflect


# take input for floor and ceiling
f = int(input('Enter starting num: '))
c = int(input('Enter ending num: '))

ie = inflect.engine()

for i in range(f, c+1):
    print(i, ie.number_to_words(i))


input('press enter to end')
