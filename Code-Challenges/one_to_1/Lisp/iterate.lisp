;; Author: Will Sinatra <wpsinatra@gmail.com>

(defun val-to-list (val)
  (let
      ((expanse () ))
    (dotimes (number val (nreverse expanse))
      (push number expanse))))

(defun str-expanse (val)
    (loop for i in (val-to-list val)
       collect (format NIL "~r" i)))

(defun concat-expanse (expanse val)
  (loop for word in expanse
     for val in (val-to-list val)
       collect (list word val)))

(defun ecase-build (expanse)
  (loop for item in expanse
     do (format t "~d~%" item)))

(defun ecase-trim (expanse &optional item)
  (setf expanse (reverse expanse))
  (loop for e in expanse while (eq item e) do (pop expanse))
  (reverse expanse))

;(ecase-build (concat-expanse (str-expanse 1000001) 1000001))
