use std::io;
use std::cmp::Ordering;

fn main() {
    let mut intlist: Vec<i64> = Vec::new();
    let mut lonelyvec: Vec<i64> = vec![0];
    println!("Enter a list of numbers to organize.");
    println!("Press enter after each number to add a new number");
    println!("Enter anything that's not a number to finish");

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)
            .expect("Failed to Read Line");
        let input: i64 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => break,
        };
        intlist.push(input);
    }

    let mut x = 0;
    loop {
        if intlist.get(x+1) != None {
            match intlist.get(x).cmp(&intlist.get(x+1)) {
                Ordering::Less => {
                    lonelyvec[0] = 0;
                    x+=1;
                },       
                Ordering::Greater => {
                    print!("[");
                    let mut bsvariable = 1;
                    for i in &intlist {
                        if bsvariable == 1 {
                            print!("{}", i);
                        } else {
                            print!(", {}", i);
                        }
                        bsvariable +=1;
                    }
                    println!("]");
                    lonelyvec[0] = intlist[x+1];
                    intlist[x+1] = intlist[x];
                    intlist[x] = lonelyvec[0];
                    x=0;
                },
                Ordering::Equal => {
                    lonelyvec[0] = 0;
                    x+=1;
                },
            };
        } else {
            println!("");
            break;
        } 
    }

    println!("Please see below for your organized vector:");
    print!("[");
    let mut anotherbsvariable = 1;
    for i in &intlist {
        if anotherbsvariable == 1 {
            print!("{}", i);
        } else {
            print!(", {}", i);
        }
        anotherbsvariable +=1;
    }
    println!("]");
    again()
}

fn again() {
    let mut mo = String::new();
    println!("\nWould you like to assort some more shit? (y/n)");
    io::stdin().read_line(&mut mo)
        .expect("Failed to Read Line");
    match mo.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => again(),
    };
}
