extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use opengl_graphics::{GlyphCache, TextureSettings};
use piston::input::*;
use chrono::prelude::*;

mod color;
mod gfx;
pub mod config;


use crate::gfx::utils::{draw_text};

pub struct App<'a> {
    pub window: config::GraphicsConfig,
    glyph_cache: GlyphCache<'a>,
    date: String,
    time: String,
}

impl <'a> App<'a>{
    pub fn new(window: config::GraphicsConfig) -> App<'a> {
        //fonts and shit
        let glyph_cache = GlyphCache::new(
            "./src/assets/trebuchet_ms.ttf",
            (),
            TextureSettings::new())
            .expect("Unable to load font");

        App {
            glyph_cache,
            window,
            date: Local::now().format("%e %b %Y").to_string(),
            time: Local::now().format("%T").to_string(),
        }
    }
    //Aang, the last Err-Render
    pub fn render(&mut self, args: &RenderArgs) {
        //Rendering Clock
        let gc = &mut self.glyph_cache;
        let date = &self.date;
        let time = &self.time;

        self.window.gl.draw(args.viewport(), |c, gl| {
            use graphics::*;

            //clear the screen
            clear(crate::color::HOTPINK, gl);

            draw_text(date, [25.0, 250.0], 128, gc, &c, gl);
            draw_text(time, [100.0, 450.0], 128, gc, &c, gl);
        });
    }
    //Gimme a Clock, not a Timestamp
    pub fn update(&mut self, _args: &UpdateArgs) {
        self.date = Local::now().format("%e %b %Y").to_string();                                                
        self.time = Local::now().format("%T").to_string();            
    }
}
