extern crate kesha;
extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
extern crate find_folder;

use piston::event_loop::*;
use piston::input::*;

use kesha::App;
use kesha::config::GraphicsConfig;

fn main() {      
    let mut app = App::new(GraphicsConfig::new(
        "But the Party Don't Stop No",
        800.0, 600.0));
    
    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut app.window.settings) {
        //RenderBender
        if let Some(r) = e.render_args() {
            app.render(&r);
        }
        if let Some(u) = e.update_args() {
            app.update(&u);
        }
    }
}
