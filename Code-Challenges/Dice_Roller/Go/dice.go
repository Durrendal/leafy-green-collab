package main

import (
	"os"
	"fmt"
	"time"
	"math/rand"
)

func roll(die int) (result int){
	seed := rand.NewSource(time.Now().UnixNano())
	handler := rand.New(seed)
	result, _ = fmt.Println(handler.Intn(die))
	return
}

func main() {
	die := os.Args[1]

	if die == "d2" {
		roll(2)
	}

	if die == "d4" {
		roll(4)
	}

	if die == "d6" {
		roll(6)
	}

	if die == "d8" {
		roll(8)
	}

	if die == "d10" {
		roll(10)
	}

	if die == "d12" {
		roll(12)
	}

	if die == "d20" {
		roll(20)
	}

	if die == "d30" {
		roll(30)
	}

	if die == "d100" {
		roll(100)
	}
}
