use std::io;
use rand::prelude::*;

struct GambleType {
    sides: u8,
}

impl GambleType {
    fn toss(self){
        let mut rng = thread_rng();
        let result = rng.gen_range(1, &self.sides + 1);
        if self.sides == 2 && result == 1 {
            println!("You flipped a coin and it landed on Heads");
        } else if self.sides == 2 && result == 2 {
            println!("You flipped a coin and it landed on Tails");
        } else {            
            println!("You rolled a {}", result);
        }
    }
}

fn main() {
    let mut input = String::new();
    println!("Select your die to roll!");
    println!("1. Coin");
    println!("2. D4");
    println!("3. D6");
    println!("4. D8");
    println!("5. D10");
    println!("6. D12");
    println!("7. D20");

    io::stdin().read_line(&mut input)
        .expect("Failed to read line");
    let input: u8 = input.trim().parse()
        .expect("Not a valid choice");    

    choice(&input);
}

fn choice(input: &u8){
    let coin = GambleType{sides: 2};
    let d4 = GambleType{sides: 4};
    let d6 = GambleType{sides: 6};
    let d8 = GambleType{sides: 8};
    let d10 = GambleType{sides: 10};
    let d12 = GambleType{sides: 12};
    let d20 = GambleType{sides: 20};

    match input {
        1 => coin.toss(),
        2 => d4.toss(),
        3 => d6.toss(),
        4 => d8.toss(),
        5 => d10.toss(),
        6 => d12.toss(),
        7 => d20.toss(),
        _ => println!("Invalid choice"),
    }
    replay(&input)
}

fn replay(input: &u8) {
    println!("\nReplay? (r)");
    println!("Change dice? (c)");
    println!("Quit? (q)");
    let mut next = String::new();    
    io::stdin().read_line(&mut next)
        .expect("Failed to read line");
    let next: char = next.trim().parse()
        .expect("Not a valid Character");
    match next {
        'r' => {
            choice(input);
        }
        'c' => main(),
        'q' => println!("Thanks for playing!"),
        _ => {
            println!("Not a valid option");
            replay(input)
        }
    }
}
