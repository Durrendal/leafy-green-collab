use std::io;
use english_numbers::convert_no_fmt;

fn main() {
    let mut input = String::new();
    println!("Enter any number between 1 and 9999999999999999999?");

    io::stdin().read_line(&mut input)
        .expect("Some Value");

    let mut input: i64 = match input.trim().parse(){
        Ok(num) => num,
        Err(_) => 0
    };

    println!("You entered:");
    println!("{}",input);
    while input >= 0 {
        println!("({} {})",convert_no_fmt(input), input);
        input -= 1;
    }
}    
    
