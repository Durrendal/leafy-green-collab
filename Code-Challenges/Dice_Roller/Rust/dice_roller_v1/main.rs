use std::io;
use rand::prelude::*;

fn main() {
    let _die = [2, 4, 6, 8, 10, 12, 20];
    let mut line = String::new();
    println!("Select your die to roll!");
    println!("1. Coin");
    println!("2. D4");
    println!("3. D6");
    println!("4. D8");
    println!("5. D10");
    println!("6. D12");
    println!("7. D20");    
    io::stdin()
        .read_line(&mut line)
        .expect("Failed to read line");
    let input: usize = line
        .trim()
        .parse()
        .expect("Wanted a number");
    choose(input - 1, _die[input - 1]);    
}

fn choose (x: usize, y: usize) {
    if x == 0 {
        flipcoin();
        play(whatnow(), x, y);
    } else {
        println!("You rolled a {}", rolldie(y));
        play(whatnow(), x, y);
    }
}

fn flipcoin() {
    print!("The coin flipped and landed on ");
    if random() {
        println!("Heads!");
    } else {
        println!("Tails");
    }
}

fn rolldie (num: usize) -> usize {
    let mut rng = thread_rng();
    return rng.gen_range(1, num+1);
}

fn whatnow() -> String{
    let mut rr = String::new();
    println!("Do it again? (y)");
    println!("Roll another die? (r)");
    println!("I'm done (n)");
    io::stdin()
        .read_line(&mut rr)
        .expect("y or n");
    return rr
}

fn play(cont: String, z: usize, sd: usize) {
    if cont.trim() == "y" {
        choose(z, sd)
    } else if cont.trim() == "r" {
        main();
    } else if cont.trim() == "n" {
        println!("Thank you for playing");
    } else {
        println!("If you can't spell you don't deserve to play again");
    }
}
