# Python Dice Roller v2 - with crit detection
from random import randint


def main():
    print("""
        DICE ROLLER v2 - With Crit Detection
        """)
    print("""
        OPTIONS:
        1: Coin
        2: 4-Sided
        3: 6-Sided
        4: 8-Sided
        5: 10-Sided
        6: 12-Sided
        7: 20-Sided
        8: Percentile Roll
        q: Quit
        """)

    choice = input('Which option are you rolling: ')

    if choice == '1':
        d2()
    elif choice == '2':
        d4()
    elif choice == '3':
        d6()
    elif choice == '4':
        d8()
    elif choice == '5':
        d10()
    elif choice == '6':
        d12()
    elif choice == '7':
        d20()
    elif choice == '8':
        percentile()
    elif choice == 'q':
        quit()
    else:
        print('Invalid choice. Please enter a vlue choice')
        main()


def d2():
    flip = randint(1, 2)
    if flip == 1:
        print('Result is Heads')
        repeat = input('Flip again? yN: ')
        if repeat.lower() == 'y':
            d2()
        else:
            main()
    elif flip == 2:
        print('Result is Tails')
        repeat = input('Flip again? yN: ')
        if repeat.lower() == 'y':
            d2()
        else:
            main()


def d4():
    print('d4 rolled: ', randint(1, 4))
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d4()
    else:
        main()


def d6():
    print('d6 rolled: ', randint(1, 6))
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d6()
    else:
        main()


def d8():
    print('d8 rolled: ', randint(1, 8))
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d8()
    else:
        main()


def d10():
    print('d10 rolled: ', randint(1, 10))
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d10()
    else:
        main()


def d12():
    print('d12 rolled: ', randint(1, 12))
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d12()
    else:
        main()


def d20():
    result = randint(1, 20)
    second_roll = False
    while second_roll is False:
        if result == 20:
            print('d20 rolled: ' +
                  str(result) + ' You got a critical! Reroll!')
            second_roll = True
            d20()
        elif result == 1:
            print('d20 rolled: ' +
                  str(result) + ' Ouch. You got a critical fail!')
            second_roll = True
        else:
            print('d20 rolled: ' + str(result))
            second_roll = True
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        d20()
    else:
        main()


def percentile():
    print('Percentage is: ' + str(randint(1, 99)) + '%')
    repeat = input('Roll again? yN: ')
    if repeat.lower() == 'y':
        percentile()
    else:
        main()


# main call
main()
