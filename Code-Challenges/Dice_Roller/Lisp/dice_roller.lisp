;; Author: Will Sinatra <wpsinatra@gmail.com>

(defmacro roll (val)
  `(random
    ,(case val
      ((d2) 2)
      ((d4) 4)
      ((d6) 6)
      ((d8) 8)
      ((d10) 10)
      ((d12) 12)
      ((d20) 20)
      ((d30) 30)
      ((d100) 100))))

(format t "To test type (roll d10)~%Die Available: d2, d4, d6, d8, d10, d12, d20, d30, d100~%")
