use std::io::{self, Write};

fn main() {
    let eggplant = String::from("🍆");
    let space = String::from("  ");
    let mut input = String::new();
    
    print!("Enter a number for the size of your pyramid: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input)
        .expect("Failed to read line");
    let input: u128 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => 5,
    };
    let mut x = input;
    let mut z = 1;
    while x > 0 {
        let mut y = x;
        while y > 0 {
            print!("{}", space);
            y -= 1;
        }
        while y < z {
            print!("{}", eggplant);
            y+=1;
        }
        z += 2;
        x -= 1;
        println!("");
    }
    again();
}

fn again() {
    let mut mo = String::new();
    println!("\nWould you like to make another pyramid? (y/n)");
    io::stdin().read_line(&mut mo)
        .expect("Failed to Read Line");
    match mo.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => again(),
    };
}

