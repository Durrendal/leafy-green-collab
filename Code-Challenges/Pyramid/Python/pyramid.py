"""
    Print pyramid based on user input
    *
   * *
  * * *
  etc
"""


def pyramid():
    n = int(input('Enter amount of rows to add to pyramid: '))

    for i in range(1, n + 1):
        print((n - i) * ' ' + i * '* ')

    repeat()


def repeat():
    r = input('Make another pyramid? yN: ')
    if r.lower() == 'y':
        pyramid()
    else:  # lazy quit
        quit()


pyramid()
