"""
    Built with inspiration from some random game I saw once that I genuinely don't know the name of but uh yeah
    Anyway, eat the green circles. Avoid the red triangles. Don't die GLHF
    TODO: add images? basket + fruits + bugs +bg = sky w/ clouds and trees
    DONEadd way for lives to go back up?
    add img for life item - uuuh heart??
    Changing comment line to test git
    Author: KBSego
"""
import turtle
import math
import random

score = 0
lives = 5

sc = turtle.Screen()
sc.title('Falling Shapes')
sc.bgcolor('lightblue')
sc.setup(width=800, height=600)
sc.tracer(0)  # load no updates


# player
class Player(turtle.Turtle):
    # initialize from parent class + set attribs
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape('turtle')
        self.speed(0)
        self.color('white')
        self.penup()
        self.goto(0, -250)
        self.setheading(90)  # facing up

    # movement
    def move_left(self):
        x = self.xcor()
        if x > -390:
            self.setx(self.xcor() - 3)

    def move_right(self):
        x = self.xcor()
        if x < 380:
            self.setx(self.xcor() + 3)

    def did_collide(self, other):
        a = self.xcor() - other.xcor()
        b = self.ycor() - other.ycor()
        distance = math.sqrt((a ** 2) + (b ** 2))

        if distance < 20:
            return True
        else:
            return False


# create instances
player = Player()


# lists
gooditems = []
baditems = []
lifeups = []

# create good items
for _ in range(20):
    gooditem = turtle.Turtle()
    gooditem.shape('circle')
    gooditem.speed(0)
    gooditem.color('green')
    gooditem.penup()
    x = random.randint(-390, 390)
    y = random.randint(300, 350)
    gooditem.goto(x, y)
    gooditem.speed = random.uniform(.25, 0.5)
    gooditems.append(gooditem)


# create bad items
for _ in range(10):
    baditem = turtle.Turtle()
    baditem.shape('triangle')
    baditem.speed(0)
    baditem.color('red')
    baditem.setheading(270)
    baditem.penup()
    x = random.randint(-390, 390)
    y = random.randint(300, 350)
    baditem.goto(x, y)
    baditem.speed = random.uniform(.25, 0.5)
    baditems.append(baditem)

# create lifeup
for _ in range(2):
    lifeup = turtle.Turtle()
    lifeup.shape('turtle')
    lifeup.speed(0)
    lifeup.color('pink')
    lifeup.setheading(270)
    lifeup.penup()
    x = random.randint(-390, 390)
    y = random.randint(300, 350)
    lifeup.goto(x, y)
    lifeup.speed = random.uniform(.25, 0.5)
    lifeups.append(lifeup)


# score and lives mechanics
pen = turtle.Turtle()
pen.speed(0)
pen.color('white')
pen.penup()
pen.goto(0, 260)
pen.ht()
font = ('Courier', 24, 'normal')
pen.write('Score: {}  Lives: {}'.format(score, lives), align='center', font=font)


# listen and keybinds
turtle.listen()
turtle.onkeypress(player.move_left, 'Left')
turtle.onkeypress(player.move_left, 'a')
turtle.onkeypress(player.move_right, 'Right')
turtle.onkeypress(player.move_right, 'd')

# mainloop
while True:
    sc.update()  # update

    # do good item falling
    for gooditem in gooditems:
        gooditem.sety(gooditem.ycor() - gooditem.speed)

        # check if off screen
        if gooditem.ycor() < -300:
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            gooditem.goto(x, y)

        # collide check
        if player.did_collide(gooditem):
            score += 10
            pen.clear()
            pen.write('Score: {}  Lives: {}'.format(score, lives), align='center', font=font)
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            gooditem.goto(x, y)

    # do bad item falling
    for baditem in baditems:
        baditem.sety(baditem.ycor() - baditem.speed)

        # check if off screen
        if baditem.ycor() < -300:
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            baditem.goto(x, y)

        # collide check
        if player.did_collide(baditem):
            lives -= 1
            pen.clear()
            pen.write('Score: {}  Lives: {}'.format(score, lives), align='center', font=font)
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            baditem.goto(x, y)

    # do life item falling
    for lifeup in lifeups:
        lifeup.sety(lifeup.ycor() - baditem.speed)

        # check if off screen
        if lifeup.ycor() < -300:
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            lifeup.goto(x, y)

        # collide check
        if player.did_collide(lifeup):
            lives += 1
            pen.clear()
            pen.write('Score: {}  Lives: {}'.format(score, lives), align='center', font=font)
            x = random.randint(-390, 390)
            y = random.randint(300, 350)
            lifeup.goto(x, y)

    if lives < 1:
        pen.clear()
        pen.color('red')
        pen.write('YOU LOSE', align='center', font=('Courier', 24, 'normal'))
        break


sc.mainloop()
