"""
    It's uh... literally Pong
    Doesn't really need an intro
    Using w+s on left and up+down on right
    Author: KBSego
"""
import turtle


sc = turtle.Screen()
sc.title('Pong')
sc.bgcolor('black')
sc.setup(width=800, height=600)
sc.tracer(0)  # stop updates, faster load

# SCOREEEEEE
score_a = 0
score_b = 0

# paddle 1
paddle_1 = turtle.Turtle()
paddle_1.speed(0)
paddle_1.shape('square')
paddle_1.shapesize(stretch_wid=5, stretch_len=1)
paddle_1.color('white')
paddle_1.penup()
paddle_1.goto(-350, 0)

# paddle 2
paddle_2 = turtle.Turtle()
paddle_2.speed(0)
paddle_2.shape('square')
paddle_2.shapesize(stretch_wid=5, stretch_len=1)
paddle_2.color('white')
paddle_2.penup()
paddle_2.goto(350, 0)

# ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape('circle')
ball.color('white')
ball.penup()
ball.goto(0, 0)
ball.dx = 0.10  # x speed in pixels
ball.dy = 0.10  # y speed in pixels

# score stuff
pen = turtle.Turtle()
pen.speed(0)
pen.color('white')
pen.penup()
pen.hideturtle()  # don't need to see turtle, just text
pen.goto(0, 260)
# write to screen, establish the fanciness
pen.write("Player A: 0 Player B: 0", align="center", font=("Courier", 24, "normal"))


# AHHHH PONG STUFF
def paddle_1_up():
    y = paddle_1.ycor()
    y += 20
    paddle_1.sety(y)


def paddle_1_down():
    y = paddle_1.ycor()
    y -= 20
    paddle_1.sety(y)


def paddle_2_up():
    y = paddle_2.ycor()
    y += 20
    paddle_2.sety(y)


def paddle_2_down():
    y = paddle_2.ycor()
    y -= 20
    paddle_2.sety(y)


# keybinds
sc.listen()
sc.onkeypress(paddle_1_up, "w")
sc.onkeypress(paddle_1_down, "s")
sc.onkeypress(paddle_2_up, "Up")
sc.onkeypress(paddle_2_down, "Down")

# game main
while True:
    sc.update()  # update

    # move ball
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

    # BORDER CHECK UP/DOWN+300/-300 L/R+400/-400
    if paddle_1.ycor() > 290:
        paddle_1.sety(290)

    if paddle_1.ycor() < -290:
        paddle_1.sety(-290)

    if paddle_2.ycor() > 290:
        paddle_2.sety(290)

    if paddle_2.ycor() < -290:
        paddle_2.sety(-290)

    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1  # reverse dir

    if ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1  # reverse dir

    # Right
    if ball.xcor() > 390:
        ball.goto(0, 0)  # ball went past paddle
        ball.dx *= -1
        score_a += 1  # ball went off, raise score
        pen.clear()
        # yeah similar to other but like format it cause too lazy to function
        pen.write("Player A: {} Player B: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))

    # Left -> basically re whatever comments above
    if ball.xcor() < -390:
        ball.goto(0, 0)
        ball.dx *= -1
        score_b += 1
        pen.clear()
        pen.write("Player A: {} Player B: {}".format(score_a, score_b), align="center", font=("Courier", 24, "normal"))

    # collision detection
    # if ball cor is between 340 and 350, set to 340 and reverse
    # if hits paddle, reverse
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_2.ycor() + 40 and ball.ycor() > paddle_2.ycor() - 40):
        ball.setx(340)
        ball.dx *= -1

    # same thing but like, the opposite
    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_1.ycor() + 40 and ball.ycor() > paddle_1.ycor() - 40):
        ball.setx(-340)
        ball.dx *= -1
