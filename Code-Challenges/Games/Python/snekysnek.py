"""
    Snake Game
    Dude it's just snake
    Like it doesn't need explaining
    It's... snake
    Author: KBSego
"""
import turtle
import time
import random


# random vars
delay = 0.1  # set delay so it's actually useable
score = 0
high_score = 0

# Screensetup
sc = turtle.Screen()
sc.title('SSSSSS IMA SNEK')
sc.bgcolor('black')
sc.setup(width=600, height=600)
sc.tracer(0)  # faster load, no updates

# Snek hed
head = turtle.Turtle()
head.speed(0)  # snek go dumy speed
head.shape('square')
head.color('white')
head.penup()
head.goto(0, 0)  # it starts here already but whatevs
head.direction = 'stop'  # start stopped until player is useful

# snek body
snekbody = []

# score
pen = turtle.Turtle()
pen.speed()
pen.color('white')
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write('Score:  0  High Score:  0', align='center', font=('Courier', 24, 'normal'))

# sneky food
food = turtle.Turtle()
food.speed(0)
food.shape('circle')
food.color('red')
food.penup()
food.goto(0, 100)  # first food static set


# Make snek functions
# also weird issue with snek going into self soo preventing that with weird != checks
def go_up():
    if head.direction != 'down':
        head.direction = 'up'


def go_down():
    if head.direction != 'up':
        head.direction = 'down'


def go_left():
    if head.direction != 'right':
        head.direction = 'left'


def go_right():
    if head.direction != 'left':
        head.direction = 'right'


def move():
    if head.direction == 'up':
        head.sety(head.ycor() + 20)

    if head.direction == 'down':
        head.sety(head.ycor() - 20)

    if head.direction == 'left':
        head.setx(head.xcor() - 20)

    if head.direction == 'right':
        head.setx(head.xcor() + 20)


# keybindigns
sc.listen()  # HEY! listen...
# yeah okay... slide to the...
sc.onkeypress(go_up, "w")
sc.onkeypress(go_down, "s")
sc.onkeypress(go_left, "a")
sc.onkeypress(go_right, "d")
sc.onkeypress(go_up, "Up")
sc.onkeypress(go_down, "Down")
sc.onkeypress(go_left, "Left")
sc.onkeypress(go_right, "Right")

# mainloop
while True:
    sc.update()  # NOW you update

    # head - border check
    # using +-290 because 600x600 300x300 blah
    if head.xcor() > 290 or head.xcor() < -290 or head.ycor() > 290 or head.ycor() < -290:
        time.sleep(1)
        head.goto(0, 0)
        head.direction = 'stop'

        # bye bye snekbody
        # there's evidently not a way to actually DELETE turtles so uh yeah
        # kinda a mem leak issue but hey small mem size and auto garbage collect so meh
        for bodypart in snekbody:
            bodypart.goto(1000, 1000)  # way the hell off screen
            bodypart.hideturtle()

        # clear list
        snekbody.clear()

        # reset delay
        delay = 0.1

        # clr score
        score = 0

        pen.clear()
        pen.write('Score:  {}  High Score:  {}'.format(score, high_score), align='center', font=('Courier', 24, 'normal'))

    # handle head/food collision -> uses Pythagorean theorem sorta
    if head.distance(food) < 20:
        # move nxt food to random spot
        # using bounds of +-290 so it doesnt go off screen. screen is 600x600
        x = random.randint(-290, 290)
        y = random.randint(-290, 290)
        food.goto(x, y)

        # add to body
        new_snekbody = turtle.Turtle()
        new_snekbody.speed(0)
        new_snekbody.shape('square')
        new_snekbody.color('white')
        new_snekbody.penup()
        snekbody.append(new_snekbody)

        # shorten delay slightly to speed up as snek gets longer
        # oooooo challenging
        delay -= 0.001

        # inc score
        score += 10

        if score > high_score:
            high_score = score

        pen.clear()
        pen.write('Score:  {}  High Score:  {}'.format(score, high_score), align='center', font=('Courier', 24, 'normal'))

    # AHHH MATH BECAUSE BODY COMPLICATED
    # no but basically using list to manipulate
    # body in reverse order to handle appending
    for i in range(len(snekbody) - 1, 0, -1):
        x = snekbody[i - 1].xcor()
        y = snekbody[i - 1].ycor()
        snekbody[i].goto(x, y)

    # uuh yeah we still have to move 0
    # which is first body part in list
    if len(snekbody) > 0:
        x = head.xcor()
        y = head.ycor()
        snekbody[0].goto(x, y)

    move()  # cmon... do something!

    # snek head and snek body collision
    for bodypart in snekbody:
        if bodypart.distance(head) < 20:
            time.sleep(1)
            head.goto(0, 0)
            head.direction = 'stop'

            for bodypart in snekbody:
                bodypart.goto(1000, 1000)  # way the hell off screen
                bodypart.hideturtle()

            # clear list
            snekbody.clear()

            # reset delay
            delay = 0.1

            # clr score
            score = 0

            pen.clear()
            pen.write('Score:  {}  High Score:  {}'.format(score, high_score), align='center', font=('Courier', 24, 'normal'))

    time.sleep(delay)  # call delay to actually be able to use snek

sc.mainloop()
