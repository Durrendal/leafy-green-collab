"""
    Space Invaders. It's Space Invaders
    Idk seemed like a cool thing to do
    Much retro
    very retro retro
    Author: KBSego
    TODO:
    clean this up, use classes and shit - copy maze
    Add images?
"""
import turtle
import os
import math
import random


# screen
sc = turtle.Screen()
sc.bgcolor('black')
sc.title('Space Turtles')
sc.tracer(0)  # load, no updates
sc.setup(width=700, height=700)

# game border - this is where the ACTION IS
border = turtle.Turtle()
border.speed(0)
border.color('white')
border.penup()
border.goto(-300, -300)  # in pixels
border.pendown()
border.pensize(3)
for side in range(4):
    border.fd(600)
    border.left(90)
border.ht()  # hide

# score
score = 0

# draw score
pen = turtle.Turtle()
pen.speed(0)
pen.color('white')
pen.penup()
pen.goto(0, 310)
pen.write('Score: 0', align='center', font=('Courier', 24, 'normal'))
border.ht()

# gameover pen
gameover = turtle.Turtle()
gameover.speed(0)
gameover.color('red')
gameover.penup()
gameover.goto(0, 0)
gameover.ht()

# player
ship = turtle.Turtle()
ship.color('blue')
ship.shape('turtle')
ship.penup()
ship.speed(0)
ship.goto(0, -250)
ship.setheading(90)

shipspeed = 15

# shooty shooty pew pew
bullet = turtle.Turtle()
bullet.color('yellow')
bullet.shape('turtle')
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5, 0.5)
bullet.ht()

bulletspeed = 1

# bullet states
# ready - ready to pew pew
# fire - pew pew
bulletstate = 'ready'

# num & list
num_of_enemies = 5
enemies = []

# add enemies to list
for i in range(num_of_enemies):
    enemies.append(turtle.Turtle())

for enemy in enemies:
    # create invaders
    enemy.color('red')
    enemy.shape('turtle')
    enemy.penup()
    enemy.speed(0)
    x = random.randint(-200, 250)
    y = random.randint(100, 250)
    enemy.goto(x, y)
    enemy.setheading(270)

enemyspeed = 0.15


# movement
def move_left():
    x = ship.xcor()
    x -= shipspeed
    if x < -280:  # handling border check
        x = -280  # 280 about as far as can go before border since ship is 20px
    ship.setx(x)


def move_right():
    x = ship.xcor()
    x += shipspeed
    if x > 280:
        x = 280
    ship.setx(x)


def fire_bullet():
    # global bullet state so it can be changed outside
    global bulletstate
    if bulletstate == 'ready':
        bulletstate = 'fire'
        # move bullet to above player
        x = ship.xcor()
        y = ship.ycor() + 10
        bullet.goto(x, y)
        bullet.st()  # show turtle


def collision(t1, t2):
    a = t1.xcor() - t2.xcor()
    b = t1.ycor() - t2.ycor()
    distance = math.sqrt((a ** 2) + (b ** 2))
    if distance < 20:
        return True
    else:
        return False


# keybind - can use ad or LR
turtle.listen()
turtle.onkeypress(move_left, 'a')
turtle.onkeypress(move_right, 'd')
turtle.onkeypress(move_left, 'Left')
turtle.onkeypress(move_right, 'Right')
turtle.onkeypress(fire_bullet, 'space')

# mainloop
while True:
    for enemy in enemies:
        # enemy movement
        x = enemy.xcor()
        x += enemyspeed
        enemy.setx(x)
        # @ edge, move back and down
        if x <= -280 or x >= 280:
            for e in enemies:
                y = e.ycor()
                y -= 40
                e.sety(y)
            enemyspeed = -enemyspeed

        # check for bullet hit enemy
        if collision(bullet, enemy):
            # reset bullet
            bullet.ht()
            bulletstate = 'ready'
            bullet.goto(1000, 1000)  # way the hell     away
            # reset enemy
            x = random.randint(-200, 250)
            y = random.randint(100, 250)
            enemy.goto(x, y)

            # update score
            score += 10
            pen.clear()
            pen.write('Score: {}'.format(score), align='center', font=('Courier', 24, 'normal'))

        # check for ship and enemy collide
        if collision(ship, enemy):
            enemy.ht()
            ship.ht()
            score = 0
            pen.clear()
            pen.write('Score: {}'.format(score), align='center', font=('Courier', 24, 'normal'))
            gameover.write('GAME OVER', align='center', font=('Courier', 24, 'normal'))
            break

    # move bullet if state is fire
    if bulletstate == 'fire':
        y = bullet.ycor()
        y += bulletspeed
        bullet.sety(y)

    # check to see if bullet fired
    if bullet.ycor() > 275:
        bullet.ht()
        bulletstate = 'ready'

    sc.update()  # now update
