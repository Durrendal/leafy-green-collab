(defparameter mult-table '())

(defun mult-step (start end)
  (let
      ((current-line ()))
    (dotimes (i (+ 1 end))
      (push (* start i) current-line))
    (print (nreverse current-line))))

(defun populate-table (max)
  (loop for i from 0 upto max
     do (push (mult-step i max) mult-table)))
