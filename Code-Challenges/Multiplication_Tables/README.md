## Challenge:

Create a simple program that prints a multiplication table up to a specified point.

ie: pass the program your max value 100 and print multiplication tables form 0-100 in the format

0 100 200 300 400 500 600...

so on and so forth.

### Additional Challenge:
Memoize the function