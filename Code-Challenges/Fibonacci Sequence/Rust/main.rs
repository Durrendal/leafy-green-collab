#[cfg(feature="std")]
extern crate core;

#[macro_use]
extern crate uint;

use std::io;
//use std::num::Wrapping;

construct_uint! {
    pub struct U1024(16);
}
    
fn main() {
    let mut input = String::new();
    println!("Who even cares what this thing actually does.");
    println!("Well...how many fiberbars you want");

    io::stdin().read_line(&mut input)
        .expect("Some sht right here");
    let mut input: u64 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };
    
    let mut a: U1024;
    let mut b: U1024 = 1.into();
    let mut c: U1024 = 0.into();
    while input > 0 {
        println!("{}", c);
        a = b;
        b = c;
        c = a+b;
        input -= 1;
    }
    again()
}

fn again() {
    let mut mo = String::new();
    println!("\nThe fiber bar was a lie, but you wanna do this weird number thing again?? (y/n)");
    io::stdin().read_line(&mut mo)
        .expect("Failed to Read Line");
    match mo.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => again(),
    };
}

