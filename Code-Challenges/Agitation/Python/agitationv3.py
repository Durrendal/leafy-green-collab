"""
    Loading bar
"""
import time


loading_bar = []


print('Please wait for something really cool...\n')


def load():
    dashes = 1
    spaces = 10
    for i in range(11):
        print('Loading[', end="")
        for x in range(dashes):
            dash = '|'
            loading_bar.append(dash)
            print(loading_bar[x], end="")
        for y in range(spaces):
            print(end=" ")
        time.sleep(1)
        dashes += 1
        spaces -= 1
        print(']')


load()

print('\nlittle longer...\n')
time.sleep(3)
print('almost there...\n')
time.sleep(3)
load()
time.sleep(5)
print('\nlol wow still waiting? Almost done...\n')
time.sleep(5)
load()
time.sleep(5)
input('\nReally surprised you waited. You satisfied? This is it. Seriously. You can close now')
