#nim c -d:release fizzbuzz

proc fizzbuzz(n: int): seq[string] =
  var
    res = newSeq[string]()

  for number in 1 .. (n + 1):
    let
      d3 = number mod 3
      d5 = number mod 5

    if d3 == 0 and d5 == 0:
      res.add("FizzBuzz")
    elif d3 == 0:
      res.add("Fizz")
    elif d5 == 0:
      res.add("Buzz")
    else:
      res.add($number)
  return res

echo fizzbuzz(15)
