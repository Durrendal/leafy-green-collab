def fizzbuzz(n: int) -> list[str]:
    res = []
    for number in range(1,n+1):
        d3 = (number % 3 == 0)
        d5 = (number % 5 == 0)
        if d3 and d5:
            res.append("FizzBuzz")
        elif d3:
            res.append("Fizz")
        elif d5:
            res.append("Buzz")
        else:
            res.append(str(number))
    return res

print(fizzbuzz(15))
