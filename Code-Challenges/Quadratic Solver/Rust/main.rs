extern crate crossterm;

use crossterm::Attribute;
use std::io::{self, Write};

fn main() {
    let mut a = String::new();
    let mut b = String::new();
    let mut c = String::new();

    println!("   Quadratic Formaula Solver");
    println!("\n         {}-b±√(b²-4ac){}",
             Attribute::Underlined,
             Attribute::NoUnderline);
    println!("     x =      2a   \n");
    println!("Enter a value for each of the 3 variables and I'll solve for x");
    print!("a: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut a)
        .expect("Failed to read");
    let a: f64 = match a.trim().parse() {
        Ok(num) => num,
        Err(_) => 0.0,
    };
    print!("b: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut b)
        .expect("Failed to read");
    let b: f64 = match b.trim().parse() {
        Ok(num) => num,
        Err(_) => 0.0,
    };
    print!("c: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut c)
        .expect("Failed to read");
    let c: f64 = match c.trim().parse() {
        Ok(num) => num,
        Err(_) => 0.0,
    };
    println!("\nYou entered {}, {}, and {}", a, b, c);
    println!("For this solution, x={} and x={}\n",
             quad_add(&a,&b,&c),
             quad_min(&a,&b,&c));
}

fn quad_add(a: &f64, b: &f64, c: &f64) -> f64{
    let result = (-b+(b*b-4.0*a*c).sqrt())/2.0*a;
    return result;
}

fn quad_min(a: &f64, b: &f64, c: &f64) -> f64{
    let result = (-b-(b*b-4.0*a*c).sqrt())/2.0*a;
    return result;
}
