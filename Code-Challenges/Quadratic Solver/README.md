Make a program that will accept input for a, b, and c and then calculate the 
solutions using the qudratic equation  

Equation:  
-b(+or-)sqrt(b^2-4ac)/2a  

Hint: it's easier to define a discriminator value that will hold b^2-4ac and 
then break apart the +or- part of the equation using your discriminator value