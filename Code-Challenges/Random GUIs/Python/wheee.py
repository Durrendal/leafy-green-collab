# spiral in turtle
from turtle import *
from random import randint


speed(0) # super fast turtle

bgcolor('black')

x = 1 # dec x

# create colours while x < 400 using rgb
while x < 400:
    r = randint(0,255)
    g = randint(0,255)
    b = randint(0,255)

    # pydocs says colormode() is not needed
    # buuuuuuuuuuuut it doesnt run without it so
    colormode(255)
    pencolor(r,g,b) # set pencolor using r,g,b vars

    forward(50 + x) # turtle forward
    right(90.911) # right using angle


    x = x+1 # add to x

exitonclick()  # lazy exit
