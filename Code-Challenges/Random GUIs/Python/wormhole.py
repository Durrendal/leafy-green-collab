# wormhole lookin thing
from turtle import *


# it's not really necesarry to name turtle but it's good practice
tiny = Turtle()

tiny.speed(10)  # dummy speed

# use for loop to change direction and make neat shape
for i in range(180):
    tiny.forward(100)
    tiny.right(30)
    tiny.forward(20)
    tiny.left(60)
    tiny.forward(50)
    tiny.right(30)
    tiny.penup()
    tiny.setposition(0, 0)
    tiny.pendown()
    tiny.right(2)

exitonclick()  # lazy exit
