#!/usr/bin/env python

from turtle import *
from random import randint

# tree turtle
def create_rectangle(turtle, color, x, y, width, height):
    turtle.penup()
    turtle.color(color)
    turtle.fillcolor(color)
    turtle.goto(x, y)
    turtle.pendown()
    turtle.begin_fill()

    turtle.forward(width)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)
    turtle.forward(width)
    turtle.left(90)
    turtle.forward(height)
    turtle.left(90)

    # fill the above shape
    turtle.end_fill()
    # Reset the orientation of the turtle
    turtle.setheading(0)

# moon turtle
def create_circle(turtle, x, y, radius, color):
    xmas.penup()
    xmas.color(color)
    xmas.fillcolor(color)
    xmas.goto(x, y)
    xmas.pendown()
    xmas.begin_fill()
    xmas.circle(radius)
    xmas.end_fill()

# background
BG_COLOR = "#004080"

xmas = Turtle()
# set turtle speed
xmas.speed(2)
screen = xmas.getscreen()
# set background color
screen.bgcolor(BG_COLOR)
# set tile of screen
screen.title("Merry Christmas")
# maximize the screen
screen.setup(width=1.0, height=1.0)

y = -100
# create tree trunk
create_rectangle(xmas, "#996633", -15, y-60, 30, 60)

# create tree
width = 240
xmas.speed(10)
while width > 10:
    width = width - 10
    height = 10
    x = 0 - width/2
    create_rectangle(xmas, "green", x, y, width, height)
    y = y + height

# create a star a top of tree
xmas.speed(1)
xmas.penup()
xmas.color('yellow')
xmas.goto(-20, y+10)
xmas.begin_fill()
xmas.pendown()
for i in range(5):
    xmas.forward(40)
    xmas.right(144)
xmas.end_fill()

tree_height = y + 40

# create moon in sky
# create a full circle
create_circle(xmas, 230, 180, 60, "white")
# make crescent
create_circle(xmas, 220, 180, 60, BG_COLOR)

# now add few stars in sky
xmas.speed(10)
number_of_stars = randint(20,30)
# print(number_of_stars)
for _ in range(0,number_of_stars):
    x_star = randint(-(screen.window_width()//2),screen.window_width()//2)
    y_star = randint(tree_height, screen.window_height()//2)
    size = randint(5,20)
    xmas.penup()
    xmas.color('white')
    xmas.goto(x_star, y_star)
    xmas.begin_fill()
    xmas.pendown()
    for i in range(5):
        xmas.forward(size)
        xmas.right(144)
    xmas.end_fill()

# print message
xmas.speed(1)
xmas.penup()
msg = "Merry Christmas"
xmas.goto(0, -200)  # y is in minus because tree trunk was below x axis
xmas.color("white")
xmas.pendown()
xmas.write(msg, move=False, align="center", font=("Arial", 15, "bold"))

xmas.hideturtle()
screen.mainloop()
