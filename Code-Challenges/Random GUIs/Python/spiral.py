import turtle

turtle.setup(800, 600)  # screen size
sc = turtle.Screen()
sc.bgcolor("black")
sc.title("Round and Round and Round...")

t = turtle.Turtle()
t.shape("turtle")
t.color("white")

t.penup()
size = 20
for i in range(30):
    t.stamp()  # it's kinda like footprints?
    size = size + 3  # turtle do a grow!
    t.forward(size)  # turtle do a forward move!
    t.right(24)  # turtle do a right turn!

sc.exitonclick()  # lazy exit
