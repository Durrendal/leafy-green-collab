"""
    I'm really not sure why
    But here's a stoplight
    Author: KBSego
"""
import turtle
import time

sc = turtle.Screen()
sc.title('Stoplight')
sc.bgcolor('black')


# box
pen = turtle.Turtle()
pen.color('yellow')
pen.width(3)
pen.ht()
pen.penup()
pen.goto(-30, 60)
pen.pendown()
for side in range(2):
    pen.fd(60)
    pen.rt(90)
    pen.fd(120)
    pen.rt(90)
pen.penup()


# redlight
red = turtle.Turtle()
red.shape('circle')
red.color('grey')
red.penup()
red.goto(0, 40)

# yellowlight
yellow = turtle.Turtle()
yellow.shape('circle')
yellow.color('grey')
yellow.penup()
yellow.goto(0, 0)

# greenlight
green = turtle.Turtle()
green.shape('circle')
green.color('grey')
green.penup()
green.goto(0, -40)

while True:
    # handle colour changes
    yellow.color('grey')
    red.color('red')
    time.sleep(4)

    red.color('grey')
    green.color('green')
    time.sleep(3)

    green.color('grey')
    yellow.color('yellow')
    time.sleep(2)

# mainloop
sc.mainloop()
