"""
    Uhh rebuilding stoplight to like use classes?
    Idk man I'm just learning by doing
    Code is longer but the only CHANGES occur in the CLASS as opposed to the individual objects
    Author: KBSego
"""
import turtle
import time


# set up screen stuff
sc = turtle.Screen()
sc.title('Stoplight')
sc.bgcolor('black')


# create class
class Stoplight(turtle.Turtle):
    # init class obj - self/x/y for parms
    def __init__(self, x, y):
        # draw stoplight
        turtle.Turtle.__init__(self)  # init parent turtle class
        self.penup()
        self.ht()  # hide turtle
        self.width(3)
        self.speed(0)
        self.color('yellow')
        self.goto(x-30, y+60)  # handle goto x/y
        self.pendown()
        for side in range(2):  # lazy rectangle
            self.fd(60)
            self.rt(90)
            self.fd(120)
            self.rt(90)

        # color tracking
        self.color = ''

        # create lights
        self.redlight = turtle.Turtle()
        self.yellowlight = turtle.Turtle()
        self.greenlight = turtle.Turtle()

        # do all the attribute stuff
        self.redlight.speed(0)
        self.yellowlight.speed(0)
        self.greenlight.speed(0)
        self.redlight.color('grey')
        self.yellowlight.color('grey')
        self.greenlight.color('grey')
        self.redlight.shape('circle')
        self.yellowlight.shape('circle')
        self.greenlight.shape('circle')
        self.redlight.penup()
        self.yellowlight.penup()
        self.greenlight.penup()
        # handle positioning redtop, yellow mid, green bot
        self.redlight.goto(x, y+40)
        self.yellowlight.goto(x, y)
        self.greenlight.goto(x, y-40)

    # handle color change
    def change_color(self, color):
        self.redlight.color('grey')
        self.yellowlight.color('grey')
        self.greenlight.color('grey')

        if color == 'red':
            self.redlight.color('red')
            self.color = 'red'  # obj color, not function
        elif color == 'yellow':
            self.yellowlight.color('yellow')
            self.color = 'yellow'
        elif color == 'green':
            self.greenlight.color('green')
            self.color = 'green'
        else:
            print('Error: Unknown Color {}'.format(color))  # ooo error handling

    # timer for changing automatically
    def timer(self):
        if self.color == 'red':
            self.change_color('green')
            sc.ontimer(self.timer, 43000)
        elif self.color == 'yellow':
            self.change_color('red')
            sc.ontimer(self.timer, 3000)
        elif self.color == 'green':
            self.change_color('yellow')
            sc.ontimer(self.timer, 2000)


# call class - create instance with x/y passed and tiemr stuff
stoplight = Stoplight(0, 0)  # origin
stoplight.change_color('red')  # self = stoplight, color = color
stoplight.timer()

stoplight2 = Stoplight(-100, 0)  # left of orig
stoplight2.change_color('yellow')
stoplight2.timer()

stoplight3 = Stoplight(100, 0)  # right of orig
stoplight3.change_color('green')
stoplight3.timer()

# mainloop
sc.mainloop()
