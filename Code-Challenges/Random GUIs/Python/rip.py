"""
    RAAAAHHH
    Figure out how to draw a friggen grave
"""
import turtle


sc = turtle.Screen()
sc.title('RIPPY')
sc.bgcolor('black')
sc.setup(width=600, height=600)  # -300to300


grave = turtle.Turtle()
grave.speed(0)
grave.color('grey')
grave.penup()
grave.goto(-140, -100)
grave.width(3)
grave.pendown()
grave.begin_fill()
grave.fd(240)
grave.rt(270)
grave.fd(200)
grave.circle(120, 180)
grave.fd(200)
grave.end_fill()
grave.fillcolor('grey')
grave.penup()
grave.color('white')
grave.goto(-45, 90)
grave.pendown()
rip = 'LOL\nRIP'
grave.write(rip, font=('Courier', 20, 'normal'))
grave.ht()


sc.mainloop()
