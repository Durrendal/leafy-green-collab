"""
    Redo of rip.py
"""
import turtle


sc = turtle.Screen()
sc.title('RIPPY')
sc.bgpic('gravebg2.gif')
sc.setup(width=1000, height=800)


# make graves
class Grave(turtle.Turtle):
    # init parent class
    def __init__(self, x, y):
        turtle.Turtle.__init__(self)
        self.speed(0)
        self.color('grey')
        self.penup()
        self.width(3)
        self.goto(x+30, y+0)  # offset
        self.pendown()
        self.begin_fill()
        self.fd(240)
        self.rt(270)
        self.fd(200)
        self.circle(120, 180)
        #print(self.position())
        self.fd(200)
        self.end_fill()
        self.fillcolor('grey')
        self.ht()


# do grave writing lol
grave = Grave(-480, -100)  # startpos and x/y
grave.penup()
grave.color('white')
grave.goto(-350, 90)  # 130 diff on x
grave.pendown()
rip1 = 'LOL\nRIP'
grave.write(rip1, font=('Courier', 20, 'normal'))
grave2 = Grave(-180, -100)
grave2.penup()
grave2.color('white')
grave2.goto(-50, 90)
grave2.pendown()
rip2 = 'UR\nDED'
grave2.write(rip2, font=('Courier', 20, 'normal'))
grave3 = Grave(130, -100)
grave3.penup()
grave3.color('white')
grave3.goto(250, 90)  # only 120 offset bc bigger words
grave3.pendown()
rip3 = 'LMAO\nRIPPY'
grave3.write(rip3, font=('Courier', 20, 'normal'))

sc.mainloop()
