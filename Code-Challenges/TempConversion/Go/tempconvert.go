package main
/* Author: Will Siantra <wpsinatra@gmail.com> */

import (
	"os"
	"fmt"
	"regexp"
	"strconv"
)

/* I tested this on a Droid4, which has a 32bit armv7 cpu, so that's why we're using float32s here */

func fahr_to_cel(val int) (sum float32) {
	sum = (float32(val) - 32.0) * 0.5555556
	return
}

func fahr_to_kel(val int) (sum float32) {
	sum = ((float32(val) - 32.0) * 0.5555556) + 273.15
	return
}

func cel_to_fahr(val int) (sum float32) {
	sum = (float32(val) * 1.80) + 32.0
	return
}

func cel_to_kel(val int) (sum float32) {
	sum = float32(val) + 273.15
	return
}

func kel_to_cel(val int) (sum float32) {
	sum = float32(val) - 273.15
	return
}

func kel_to_fahr(val int) (sum float32) {
	sum = ((float32(val) - 273.15) * 1.80) + 32.0
	return
}

func main() {
	argv1 := os.Args[1]
	argv2 := os.Args[2] 
	isCel, _ := regexp.MatchString("c", argv2)
	isFahr, _ := regexp.MatchString("f", argv2)
	isKel, _ := regexp.MatchString("k", argv2)
	val, _ := strconv.Atoi(argv1)

	if isCel == true {
		c_to_f := fmt.Sprintf("%f", cel_to_fahr(val))
		c_to_k := fmt.Sprintf("%f", cel_to_kel(val))
		fmt.Println("Fahr: " + c_to_f, "Kel: " + c_to_k)
	}

	if isFahr == true {
		f_to_c := fmt.Sprintf("%f", fahr_to_cel(val))
		f_to_k := fmt.Sprintf("%f", fahr_to_kel(val))
		fmt.Println("Cel: " + f_to_c, "Kel: " + f_to_k)
	}

	if isKel == true {
		k_to_f := fmt.Sprintf("%f", kel_to_fahr(val))
		k_to_c := fmt.Sprintf("%f", kel_to_cel(val))
		fmt.Println("Fahr: " + k_to_f, "Cel: " + k_to_c)
	}
}
