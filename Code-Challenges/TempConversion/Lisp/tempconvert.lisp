;; Author: Will Sinatra <wpsinatra@gmail.com>

(defun F-to-C (value)
  (format NIL "Celsius: ~a"
	  (* (- value 32) 0.5555556)))

(defun F-to-K (value)
  (format NIL "Kelvin: ~a"
	  (+ (* (- value 32) 0.5555556) 273.15)))

(defun C-to-F (value)
  (format NIL "Faremheit: ~a"
	  (+ (* value 1.80) 32)))

(defun C-to-K (value)
  (format NIL "Kelvin: ~a"
	  (+ value 273.15)))

(defun K-to-F (value)
  (format NIL "Farenheit: ~a"
	  (+ (* (- value 273.15) 1.80) 32)))

(defun K-to-C (value)
  (format NIL "Celsius: ~a"
	  (- value 273.15)))

(defun conversions-of (val typ)
  (format NIL "~a~a is equal to:~% ~%~a~%~a~%" val typ
	  (cond
	    ((equal "f" typ)
	       (F-to-C val))
	    ((equal "c" typ)
	       (C-to-F val))
	    ((equal "k" typ)
	     (K-to-F val)))
	  (cond
	    ((equal "f" typ)
	     (F-to-K val))
	    ((equal "c" typ)
	     (C-to-K val))
	    ((equal "k" typ)
	     (K-to-C val)))))
