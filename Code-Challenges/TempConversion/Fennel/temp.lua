#!/usr/bin/env lua

local function F_to_C(v)
  return ((v - 32) * 0.55555560000000004)
end
local function F_to_K(v)
  return (((v - 32) * 0.55555560000000004) + 273.14999999999998)
end
local function C_to_F(v)
  return ((v * 1.8) + 32)
end
local function C_to_K(v)
  return (v + 273.14999999999998)
end
local function K_to_F(v)
  return (((v - 273.14999999999998) * 1.8) + 32)
end
local function K_to_C(v)
  return (v - 273.14999999999998)
end
local function conversions_of(v, typ)
  local ttyp = ""
  print((v .. typ .. " is equal to:"))
  if (typ == "K") then
    ttyp = "k"
  elseif (typ == "F") then
    ttyp = "f"
  elseif (typ == "C") then
    ttyp = "c"
  elseif (typ == "kelvin") then
    ttyp = "k"
  elseif (typ == "farenheit") then
    ttyp = "f"
  elseif (typ == "celsius") then
    ttyp = "c"
  elseif (typ == "Kelvin") then
    ttyp = "k"
  elseif (typ == "Farenheit") then
    ttyp = "f"
  elseif (typ == "Celsius") then
    ttyp = "c"
  end
  if (ttyp == "f") then
    print(("Celsius: " .. F_to_C(v)))
    return print(("Kelvin: " .. F_to_K(v)))
  elseif (ttyp == "c") then
    print(("Farenheit: " .. C_to_F(v)))
    return print(("Kelvin: " .. C_to_K(v)))
  elseif (ttyp == "k") then
    print(("Farenheit: " .. K_to_F(v)))
    return print(("Celsius: " .. K_to_C(v)))
  end
end
return conversions_of(arg[1], arg[2])
