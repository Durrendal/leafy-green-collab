#!/usr/bin/fennel
;;Author: Will Sinatra <wpsinatra@gmail.com>

(fn F-to-C [v]  
  (* (- v 32) 0.5555556))

(fn F-to-K [v]
  (+ (* (- v 32) 0.5555556) 273.15))

(fn C-to-F [v] 
  (+ (* v 1.80) 32))

(fn C-to-K [v]
  (+ v 273.15))

(fn K-to-F [v]
  (+ (* (- v 273.15) 1.80) 32))

(fn K-to-C [v]
  (- v 273.15))

(fn conversions-of [v typ]
  (var ttyp "")
  (print (.. v typ " is equal to:"))
  (if
   (= typ "K")
   (set ttyp "k")
   (= typ "F")
   (set ttyp "f")
   (= typ "C")
   (set ttyp "c")
   (= typ "kelvin")
   (set ttyp "k")
   (= typ "farenheit")
   (set ttyp "f")
   (= typ "celsius")
   (set ttyp "c")
   (= typ "Kelvin")
   (set ttyp "k")
   (= typ "Farenheit")
   (set ttyp "f")
   (= typ "Celsius")
   (set ttyp "c"))
  (if
   (= ttyp "f")
   (do
     (print (.. "Celsius: " (F-to-C v)))
     (print (.. "Kelvin: " (F-to-K v))))
   (= ttyp "c")
   (do
     (print (.. "Farenheit: " (C-to-F v)))
     (print (.. "Kelvin: " (C-to-K v))))
   (= ttyp "k")
   (do
     (print (.. "Farenheit: " (K-to-F v)))
     (print (.. "Celsius: " (K-to-C v))))))

(conversions-of (. arg 1) (. arg 2))
