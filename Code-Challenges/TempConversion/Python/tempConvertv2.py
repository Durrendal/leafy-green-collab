# F to C to K conversions


def main():
    print("""
        Conversions
        1: F -> C
        2: C -> F
        3: F -> K
        4: K -> F
        5: C -> K
        6: K -> C
        q: Quit
        """)

    convMethod = input('Choose a conversion: ')

    if convMethod == '1':
        print('Converting Fahrenheit to Celcius')
        finput = int(input('Enter the input temperature in Fahrenheit: '))
        coutput = int(round(finput - 32) * 5 / 9)

        print(str(finput) + 'F is ' + str(coutput) + 'C')

    elif convMethod == '2':
        print('Converting Celcius to Fahrenheit')
        cinput = int(input('Enter the input temperature in Celcius: '))
        foutput = int(round((9 * cinput) / 5 + 32))

        print(str(cinput) + 'C is ' + str(foutput) + 'F')

    elif convMethod == '3':
        print('Converting Fahrenheit to Kelvin')
        fkinput = int(input('Enter the input temperature in Fahrenheit: '))
        kfoutput = int(round((fkinput - 32) * (5 / 9) + 273.15))

        print(str(fkinput) + 'F is ' + str(kfoutput) + 'K')

    elif convMethod == '4':
        print('Converting Kelvin to Fahrenheit')
        kfinput = int(input('Enter the input temperature in Kelvin: '))
        fkoutput = int(round((kfinput - 273.15) * (9 / 5) + 32))

        print(str(kfinput) + 'K is ' + str(fkoutput) + 'F')

    elif convMethod == '5':
        print('Converting Calcius to Kelvin')
        ckinput = int(input('Enter the input temperature in Celcius: '))
        kcoutput = int(round(ckinput + 273.15))

        print(str(ckinput) + 'C is ' + str(kcoutput) + 'K')

    elif convMethod == '6':
        print('Converting Kelvin to Celcius')
        kcinput = int(input('Enter the input temperatture in Kelvin: '))
        ckoutput = int(round(kcinput - 273.15))

        print(str(kcinput) + 'K is ' + str(ckoutput) + 'C')

    elif convMethod == 'q':
        quit()

    r = input('More conversions? yN')
    if r.lower() == 'y':
        main()
    else:  # lazy else
        quit()


main()
