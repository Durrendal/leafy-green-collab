use std::io;

fn main() {
    let mut input = String::new();
    println!("What Temperature  would you like to convert?");
    println!("1. Fahrenheit to Celsius");
    println!("2. Fahrenheit to Kelvin");
    println!("3. Celsius to Fahrenheit");
    println!("4. Celsius to Kelvin");
    println!("5. Kelvin to Fahrenheit");
    println!("6. Kelvin to Celsius");

    io::stdin().read_line(&mut input)
        .expect("Failed to Read Line");
    let input: u8 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => 0,
    };
    println!("Please enter a temperature");
    convert_temp(input);
    convert_again();
}

fn convert_temp(input: u8){
    let mut temp = String::new();
    io::stdin().read_line(&mut temp)
        .expect("Failed to Read Line");
    let temp: f32 = match temp.trim().parse() {
        Ok(num) => num,
        Err(_) => 0.0};
        
    match input {
        1 => f_to_c(&temp),
        2 => f_to_k(&temp),
        3 => c_to_f(&temp),
        4 => c_to_k(&temp),
        5 => k_to_f(&temp),
        6 => k_to_c(&temp),
        _ => println!("Did not choose a valid option, wasting your time successful")
    }
}

fn f_to_c(temp: &f32){
    let newtemp = (temp - 32.0) * 5.0 / 9.0;
    println!("{} degrees Fahrenheit is {} degrees Celcius", temp, newtemp);
    
}

fn f_to_k(temp: &f32){
    let newtemp = ((temp - 32.0) * 5.0 / 9.0) + 273.15;
    println!("{} degrees Fahrenheit is {} degrees Kelvin", temp, newtemp);    
}

fn c_to_f(temp: &f32){
    let newtemp = (temp + 32.0) * 9.0 / 5.0;
    println!("{} degrees Celsius is {} degrees Fahrenheight", temp, newtemp);    
}
        
fn c_to_k(temp: &f32){
    let newtemp = temp + 273.15;
    println!("{} degrees Celsius is {} degrees Kelvin", temp, newtemp);    
}

fn k_to_f(temp: &f32){
    let newtemp = ((temp - 273.15) * 9.0 / 5.0) + 32.0;
    println!("{} degrees Kelvin is {} degrees Fahrenheit", temp, newtemp);    
}

fn k_to_c(temp: &f32){
    let newtemp = temp - 273.15;
    println!("{} degrees Kelvin is {} degrees Celsius", temp, newtemp);    
}

fn convert_again() {
    let mut again = String::new();
    println!("\nWould you like to convert another temperature? (y/n)");
    io::stdin().read_line(&mut again)
        .expect("Failed to Read Line");
    match again.trim() {
        "y" => {
            println!("");
            main();
        },
        "n" => println!("Toodaloo"),
        _  => convert_again(),
    };
}
