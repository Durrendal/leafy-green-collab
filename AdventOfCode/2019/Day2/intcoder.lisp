(ql:quickload "str")

;;===== OpCodes =====

(defparameter *99* "HALT")

(defparameter *1* "ADD")

(defparameter *2* "MULT")

;; 1202 Problem state + opcode correction
(defparameter *state* "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,9,1,19,1,19,5,23,1,23,5,27,2,27,10,31,1,31,9,35,1,35,5,39,1,6,39,43,2,9,43,47,1,5,47,51,2,6,51,55,1,5,55,59,2,10,59,63,1,63,6,67,2,67,6,71,2,10,71,75,1,6,75,79,2,79,9,83,1,83,5,87,1,87,9,91,1,91,9,95,1,10,95,99,1,99,13,103,2,6,103,107,1,107,5,111,1,6,111,115,1,9,115,119,1,119,9,123,2,123,10,127,1,6,127,131,2,131,13,135,1,13,135,139,1,9,139,143,1,9,143,147,1,147,13,151,1,151,9,155,1,155,13,159,1,6,159,163,1,13,163,167,1,2,167,171,1,171,13,0,99,2,0,14,0")

;;===== OpCode Reader =====

(defun listify (str)
  "Turn string to list"
  (with-input-from-string (in str)
    (loop for x = (read in nil nil) while x collect x)))

(defun op (op)
  "Listify opcode string"
  (listify (str:replace-all "," " " op)))

(defparameter *lstate* (op *state*))

(defparameter *determined* (list))
;List of determined opcodes in lazy global scope

(defun detop (op)
  "Determine which items in opcode string are ops"
  (loop for n from 0 to (length op)
     do
       (cond
	 ((equal (nth n op) 1)
	  (push n *determined*)
	  (setf n (+ 3 n)))
	 ((equal (nth n op) 2)
	  (push n *determined*)
	  (setf n (+ 3 n)))
	 ((equal (nth n op) 99)
	  (push n *determined*))))
  (reverse *determined*))

(defun parse (op)
  "Set *determined* to nil, determine opcodes, parse opcodes"
  (setf *determined* (list))
  (detop op)
  (format t "DetOps: ~a~%" (reverse *determined*))
    (loop for n in (reverse *determined*)
       do
	 (progn
	   (let
	       ((pval (nth n op)))
	     (cond
	       ((equal pval 99)
		(format t "HALT || POS: ~a VAL: ~a~%" n pval)
		(return))
	       ((equal pval 1)
		(format t "ADD || POS: ~a VAL: ~a~%" n pval)
		(setf
		 (nth (nth (+ 3 n) op) op) (+ (nth (nth (+ 1 n) op) op) (nth (nth (+ 2 n) op) op))))
	       ((equal pval 2)
		(format t "MULT || POS: ~a VAL: ~a~%" n pval)
		(setf
		 (nth (nth (+ 3 n) op) op) (* (nth (nth (+ 1 n) op) op) (nth (nth (+ 2 n) op) op))))
	       (t (format t "NOT OP || POS: ~a VAL:~a~%" n pval))))))
    (format t "~a~%" op)
    (values))

(defun checkop (code segment segment2)
  (loop for n from 1 to 99
	do
	(progn
	  (setf (nth segment *lstate*) n)
	  (if (equal (nth 0 (parse *lstate*)) code)
	      (format t "~a~%" *lstate*)))))
	    ;(progn
	    ;  (setf (nth segment2 *lstate*) n)
	    ;  (if (equal (nth 0 (parse *lstate*)) code)
	    ;  (format t "~a~%" *lstate*)))))))
	    

(defun toopc (oplist)
  "Convert spaced string to opcode"
  (str:replace-all " " "," oplist))
